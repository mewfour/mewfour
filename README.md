# README #



### What is this repository for? ###

This is a 2D multiplayer battle arena game built in Corona sdk. 

### How do I get set up? ###

Download the Corona SDK from https://developer.coronalabs.com/downloads/coronasdk. You have to register a account in order to download the sdk.

Once installed, clone the repository to local disk, open the Corona Simulater and choose open project, choose the main.lua in the CatFigter folder so you can open the project Cat Fighter.

The client part is in Client folder, the server is in Server folder. In order to play the game, you need to open three simulators, one running the server code and the other two running the client code. NOTE: the server will reject multiple connections from a single IP address. i.e. the two clients need to have different IP addresses in order for the server to work properly. **For clients to find the server, change the address and port in function scene:show in CatFighter/scenes/menu.lua to your ip and port of server.**

After testing in the simulater can use it to automatically make a build for Android system. In Corona Simulator choose File -> Build -> Android and choose the directory you want to build to. It will generate a apk file then you can directly install the game to your phone.

### Contribution guidelines ###

Classes folder contains all the necessary classes in the gameplay, including the player class, the controller class that use on screen buttons to control the player, the potion that get spawned in the map, the weapons used for attack, and the opponent class used for representing the opponent.

Images folder contains all the png images needed for the game (all the needed buttons, sprites for item class, sprites for the animation of the player, background, platform, etc).

libs folder has the packages that are available from internet and are helpful to our project, and also some other functions to be called in the program such as the test packages and textController which updates the chat text displayed in game.  

Scenes has all the scenes for the game, waiting is for the waiting scene, win and lose are for the end game, replay is for replaying the game. Once the Project is loaded, simulator opens the menu.lua from main, then can go to different scenes from menu, accorrding to the code. 


For the server, basically all the message receiving and sending are in main.lua. libs folder holds the encoder for messages and loadsave for retriving the player profile in server's local directory. 

Decided to write test packages for each class which needs to test. The test packages just contains a bunch of assertion that make sure the classes work in the way they should. They are already imported into the classes which require them. In player.lua it uses playerTest and some of itemTest to test the player movement and weapon usage. The replayTest is used in game.lua and replay.lua to test the information storing and retriving. ItemTest is used in game.lua to test the item generation, etc.




### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact