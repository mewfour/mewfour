local socket = require("socket")
local json = require("json")
local loadsave = require('libs.loadsave')
local encoder = require('libs.easyEncoder')


-- Create udp connection. 
udp=socket.udp()

udp:setsockname("10.13.191.34", 22345)
udp:settimeout(0)

ips = {}   -- Stores all IPs that are connecting to the server. 
ports = {} -- Stores all IPs and their port numbers. 

counters = {} -- Stores timeout timers for all IPs

WAITING_INITIAL = 7000 
WAITING_COUNTER = 1000

waitingIp = nil      -- Stores the IP that is currently waiting to be matched. 
waitingPort = nil    -- Stores the port number of the waiting IP
waitingIpCounter = WAITING_INITIAL 
math.randomseed(os.time())

spawnTimer = 300
spawnReady = false

local debug = false

-- Send message to specific ip and port
local function sendMsg(data, ip, port) 
	udp:sendto(data, ip, port)
end

-- Find the value that has specified key and not equals to the given port number
local function findVal(table, key, port)
	for i=1, #table do
		if (table[i][1]== key and i < #table) then
			if(table[i+1][1] == key) then
				if (table[i][2] ~= port) then
					-- returns the first port number
					return table[i][2]
				else
					-- returns the second port number
					return table[i+1][2]
				end
			end
		end
		if (table[i][1] == key) then
			return table[i][2]
		end
	end
	return nil
end

-- Find index of an element in table
local function findIndex(table, element)
	for i=1, #table do
		if (table[i]==element) then
			return i
		end
	end
	return nil
end

-- Print all elements in a list
local function printList(list)
	for i=1,#list do
		print(list[i])
	end
end

-- Send a message repeatedly 10 times every 200ms 
local function repeatMsg(message, ip, port)
	local count = 10
	local cooldown = false
	local function resetCooldown()
		cooldown = false
	end
	while (count > 0 and cooldown == false) do
		if (debug) then print(count) end
		cooldown = true
		sendMsg(message, ip, port)
		count = count - 1
		timer.performWithDelay(200, resetCooldown)
	end
end

-- Client matching function
local function matching(ip, port)
	print("Connection from: ", ip, port)

	-- First client found (Sending multiple messages to ensure client receives)
	if (waitingIp==nil) then
		waitingIp = ip
		waitingPort = port
		waitingIpCounter = WAITING_INITIAL
		repeatMsg("1", ip, port)
		sendMsg("1", ip, port)
		sendMsg("1", ip, port)
		sendMsg("1", ip, port)
		sendMsg("1", ip, port)
		sendMsg("1", ip, port)
		sendMsg("1", ip, port)
		sendMsg("1", ip, port)
		sendMsg("1", ip, port)
		sendMsg("1", ip, port)
		print("First client found")

	-- Second client found (Sending multiple messages to ensure client receives)
	else
		repeatMsg("2", ip, port)
		sendMsg("2", ip, port)
		sendMsg("2", ip, port)
		sendMsg("2", ip, port)
		sendMsg("2", ip, port)
		sendMsg("2", ip, port)
		sendMsg("2", ip, port)
		sendMsg("2", ip, port)
		-- store the two IPs
		table.insert(ports, #ports+1, {waitingIp, waitingPort})
		table.insert(ports, #ports+1, {ip, port})
		table.insert(ips, #ips+1, waitingIp)
		table.insert(ips, #ips+1, ip)
		if (debug) then
			print(#ports)
			print(ports[1])
			print(ports[2])
			print(#ips)
			print(ips[1])
			print(ips[2])
			print(ips[0])
			print("send 3 to")
		end
		local ip_index = findIndex(ips, ip)-1
		if (ip_index == 0) then
			ip_index = 2
		end
		local player1_ip = ips[ip_index]
		local player1_port = findVal(ports, ips[ip_index], port)
		--local function delayMsg()
		if (debug) then
			print(player1_ip)
			print(player1_port)
		end

		-- first player can now move to game screen
		repeatMsg("3",player1_ip, player1_port)
		sendMsg("3",player1_ip, player1_port)
		sendMsg("3",player1_ip, player1_port)
		sendMsg("3",player1_ip, player1_port)
		sendMsg("3",player1_ip, player1_port)
		sendMsg("3",player1_ip, player1_port)
		sendMsg("3",player1_ip, player1_port)
		
		if (debug) then
			print("Player1's ip " ..player1_ip)
			print("Player1's port ".. player1_port)
			--sendMsg("3", player1_ip, player1_port)
		end
		table.insert(counters, #counters+1, WAITING_COUNTER)
		table.insert(counters, #counters+1, WAITING_COUNTER)
		waitingIp = nil
		waitingPort = nil
		print("Second client found")
		print("new pair made")
	end
end

-- retrieve profile from database
local function retrieveProfile(playerData)
	data = loadsave.loadTable(playerData.playerId .. ".json", 
		   system.DocumentsDirectory)
	if data then
	    if data.password == playerData.password then
	    	return data
	    end
	end
	return nil	
end

-- create profile in database
local function createProfile(playerData)
	if (loadsave.loadTable(playerData.playerId .. ".json", 
		system.DocumentsDirectory) == nil) then

	    loadsave.saveTable(playerData, playerData.playerId .. ".json", 
	    	system.DocumentsDirectory)
	    return true
	else
		return false
	end
end

-- save profile to database (update)
local function saveProfile(playerData)
	loadsave.saveTable(playerData, playerData.playerId .. ".json", system.DocumentsDirectory)
	return true
end

-- Enter frame listener
-- This function is called once when enter a new frame. 
local function eachFrame(e)

	-- Update timeout timer for all conncted IPs
	for i=1, #counters do
		counters[i] = counters[i] - 1
	end
	if (waitingIp~=nil) then
		waitingIpCounter = waitingIpCounter - 1
	end

	-- Receive message
	data, ip, port = udp:receivefrom()
	if data then
		local decoded = encoder.decode(data)
		-- message for initial connection
		if (data == "connect") then
		    sendMsg("connected", ip, port)
		-- message for starting a game
		elseif (data == "start") then
			matching(ip, port)
		-- message indicating the client is still active
		elseif (data == "hello") then
			if (findIndex(ips, ip)~=nil and waitingIp~=ip) then
				counters[findIndex(ips, ip)] = WAITING_COUNTER
			end
			if (waitingIp==ip) then
				waitingIpCounter = WAITING_COUNTER
			end
		-- 
		elseif (data == "opponentlose") then
			local i = findIndex(ips, ip)
			print("opponent lose")
        	if ((i%2)~=0) then -- odd
				sendMsg("lose", ips[i+1], findVal(ports, ips[i+1], port)) -- send lose msg to opponent
				print(ips[i+1] .. "lose")
				table.remove(ports, i)
				table.remove(ports, i)
				print("removed", ips[i])
				table.remove(ips, i)
				print("removed", ips[i])
				table.remove(ips, i)
				table.remove(counters, i)
				table.remove(counters, i)
				print("ips")
				printList(ips)
			else 
				sendMsg("lose", ips[i-1], findVal(ports, ips[i-1], port))
				print(ips[i-1] .. "lose")
				table.remove(ports, i)
				table.remove(ports, i-1)
				print("removed", ips[i])
				table.remove(ips, i)
				print("removed", ips[i-1])
				table.remove(ips, i-1)
				table.remove(counters, i)
				table.remove(counters, i-1)
				print("ips")
				printList(ips)
			end
			sendMsg("win", ip, port)
		elseif (decoded.type == "createProfile") then
			local profile = createProfile(decoded)
			if (profile == true) then
				sendMsg("createSuccess", ip, port)
			else
				sendMsg("createFail", ip, port)
			end
		elseif (decoded.type == "login") then
			local profile = retrieveProfile(decoded)
			if (profile ~= nil) then
				sendMsg("loginSuccess", ip, port)
				sendMsg(encoder.encode(profile), ip, port)
            else
            	sendMsg("loginFail", ip, port)
            end
        elseif (decoded.type == "saveProfile") then
			local profile = saveProfile(decoded)
            sendMsg("saveSuccess", ip, port)
        -- 
        elseif (findIndex(ips, ip)~=nil) then
        	-- if message is from an IP with an odd index, 
        	-- pass the message to the IP that is after it
			if ((findIndex(ips, ip)%2)~=0) then -- odd
				sendMsg(data, ips[findIndex(ips, ip)+1], 
				    findVal(ports, ips[findIndex(ips, ip)+1], port))
			-- if message is from an IP with an even index, 
        	-- pass the message to the IP that is before it
			else 
				sendMsg(data, ips[findIndex(ips, ip)-1], 
					findVal(ports, ips[findIndex(ips, ip)-1], port))
			end
			if (debug) then
				msg = encoder.decode(data)
				if (msg.deleteItem=="1") then
					print("delete item 1")
				elseif (msg.deleteItem=="2") then
					print("delete item 2")
				end
			end
		end
	end

	-- If the the timer of the waiting IP reaches 0, it is treated as disconnected 
	-- and will be deleted. 
	if (waitingIpCounter<=0) then
		waitingIp = nil
		waitingIpCounter = WAITING_COUNTER
		print("waiting ip", waitingIp, "removed")
	end 

	-- If any IP did not send any message to the server for a certain period of time, 
	-- it is considered disconnected and will be deleted. Its opponent will also be 
	-- deleted after an announcement is sent. 
	for i=1,#ips do
		if (counters[i]~=nil and counters[i]<=0) then
			if (debug) then
				print("before")
				printList(ips)
				print("ip: ", ips[i])
			end
			if ((i%2)~=0) then -- odd index
				print("opponent disconnected")
				sendMsg("Opponent disconnected", ips[i+1], 
				    findVal(ports, ips[i+1], port)) 
				table.remove(ports, i)
				table.remove(ports, i)
				print("removed", ips[i])
				table.remove(ips, i)
				print("removed", ips[i])
				table.remove(ips, i)
				table.remove(counters, i)
				table.remove(counters, i)
			else -- even index
				sendMsg("Opponent disconnected", ips[i-1], 
					findVal(ports, ips[i-1], port))
				print("opponent disconnected")
				table.remove(ports, i)
				table.remove(ports, i-1)
				print("removed", ips[i])
				table.remove(ips, i)
				print("removed", ips[i-1])
				table.remove(ips, i-1)
				table.remove(counters, i)
				table.remove(counters, i-1)
			end
			if (debug) then 
				print("after")
				printList(ips)
			end
		end
	end

	-- if any game is in progress, decrease the spawn timer. 
	if (#ips >= 2 ) then
		spawnTimer = spawnTimer - 1
	end

	if (spawnTimer <= 0) then
		print("Spawn item")
		spawn_item()
	end
end

function spawn_item()
	for i=1,#ips,2 do
			randomValue = math.random()
			-- generate a random position to spawn item
			spawnPos = math.ceil(randomValue*1000) 
			if (debug) then print(spawnPos) end

			local message
			if (randomValue <0.5) then
				message = {direction = "server",
								 item = "potion", 
								 itemPos = spawnPos}
			else 
				message = {direction = "server",
								 item = "boxing", 
								 itemPos = spawnPos}
			end
			-- send item information to both IPs
			sendMsg(encoder.encode(message), ips[i], findVal(ports, ips[i]))
			sendMsg(encoder.encode(message), ips[i+1], findVal(ports, ips[i+1]))
			if (debug) then
				print (findVal(ports, ips[i]))
				print (ips [i])
				print (findVal(ports, ips[i+1]))
				print (ips [i+1])
			end
			-- reset spawnTimer
			spawnTimer = 300 + randomValue*300
	end

end

Runtime:addEventListener("enterFrame", eachFrame)
