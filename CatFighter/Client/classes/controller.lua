--Controller class


local composer = require('composer')
local widget = require('widget')
local relayout = require('libs.relayout')

local _M = {}
local isChatBtnOn = true
local chatBox = {}
local visualButton = {}
local chatBackground



local _W,_H, _CX, _CY = relayout._W, relayout._H, relayout._CX, relayout._CY

local function updateAndDisplayChatBox()

	chatBackground.alpha = 0.2

	chatBox.textField.x = _W + 500
	chatBox.textField.y = _H + 500
	chatBox.chatBtn.isVisible = false
	chatBox.rtnBtn.isVisible = false
	chatBox.resume.isVisible = false
	chatBackground.isVisible = false

	visualButton.atkBtn.isVisible = false
	visualButton.jumpBtn.isVisible = false
	visualButton.leftBtn.isVisible = false
	visualButton.rightBtn.isVisible = false
	visualButton.itemSLot.isVisible = false

	if (isChatBtnOn) then
		chatBox.chatBtn.isVisible = true

		visualButton.atkBtn.isVisible = true
		visualButton.jumpBtn.isVisible = true
		visualButton.leftBtn.isVisible = true
		visualButton.rightBtn.isVisible = true
		visualButton.itemSLot.isVisible = true

	else
		chatBackground.isVisible = true
		chatBox.textField.x = _CX
		chatBox.textField.y = _H - 30

		chatBox.rtnBtn.isVisible = true
		chatBox.resume.isVisible = true
	end
end



function _M.newController(scene,player,playerPos,opponent)

	local controller = display.newGroup()


	chatBackground = display.newRect(display.screenOriginX, display.screenOriginY, _W * 2, _H* 2)
	chatBackground:setFillColor(0,0,0)
	chatBackground.isVisible = false
	controller:insert(chatBackground)

	--print(playerPos)

	local maxHealth = player:getHealth()
	--print(maxHealth)
	local maxOpHealth = opponent:getHealth()
	--print(maxOpHealth)
	local healthBar1
	local damageBar1
	local healthBar2
	local damageBar2

	healthBar1 = display.newRect(display.screenOriginX + maxHealth*3,display.screenOriginY + _H/10, maxHealth * 3, _H / 20)
	damageBar1 = display.newRect(display.screenOriginX + maxHealth*(3+(3/2)),display.screenOriginY + _H/10, 0, _H / 20)


	healthBar2 = display.newRect(_W - maxOpHealth*3,display.screenOriginY + _H/10, maxOpHealth * 3, _H / 20)
	damageBar2 = display.newRect(_W - maxOpHealth*(3+(3/2)),display.screenOriginY + _H/10, 0, _H / 20)

	healthBar1:setFillColor( 000/255, 255/255, 0/255 )
	healthBar1.strokeWidth = 1
	healthBar1:setStrokeColor( 255, 255, 255, .5 )

	damageBar1:setFillColor( 255/255, 0/255, 0/255 )

	controller:insert(healthBar1)
	controller:insert(damageBar1)




	healthBar2:setFillColor( 000/255, 255/255, 0/255 )
	healthBar2.strokeWidth = 1
	healthBar2:setStrokeColor( 255, 255, 255, .5 )

	damageBar2:setFillColor( 255/255, 0/255, 0/255 )

	controller:insert(healthBar2)
	controller:insert(damageBar2)

	function updateDamageBar1()
		local currentHealth = 0
		if(playerPos == "1") then
			currentHealth = player:getHealth()

		elseif(playerPos == "2") then
			currentHealth = opponent:getHealth()
		end
		damageBar1.x = display.screenOriginX + maxHealth*(3+(3/2)) - (maxHealth - currentHealth)*3/2
		damageBar1.width = (maxHealth - currentHealth) * 3

	end

	function updateDamageBar2()
		local currentHealth = 0
		if(playerPos == "1") then
			currentHealth = opponent:getHealth()
		elseif(playerPos == "2") then
			currentHealth = player:getHealth()
		end
		damageBar2.x = _W - maxOpHealth*(3+(3/2)) + (maxOpHealth - currentHealth)*3/2
		damageBar2.width = (maxOpHealth - currentHealth) * 3

	end

	chatBox.rtnBtn = widget.newButton{
		defaultFile = "images/buttons/restart.png",
		overFile = "images/buttons/restart-over.png",
		width = 40, height = 40,
		x = _W - 30 , y = 30,
		onRelease = function ()
			controller:removeSelf()
			composer.gotoScene("scenes.menu","fade",500)
		end
	}
	controller:insert(chatBox.rtnBtn)
	chatBox.rtnBtn.isVisible = false

	local moveLeft = false
	local moveRight = false
	local wasMoving = false
	local jump = false
	local attack = false
	local wasAtking = false

	function handleEnterFrame(event)
		if (moveLeft == true) then
			player:controlLeft()
		end

		if(moveRight == true) then
			player:controlRight()
		end

		if(wasMoving == true) then
			player:stop()
			wasMoving = false
		end


		if(jump == true) then
			player:controlJump()
		end
		if(attack  == true) then
			player:setAttackTrue()
		elseif(wasAtking == true) then
			player:setAttackFalse()
			wasAtking = false
			--print("atk false")
		end
		updateDamageBar1()
		updateDamageBar2()


	end

	Runtime:addEventListener( "enterFrame", handleEnterFrame )

	local function handleLeftBtn(event)
		--print("l e")
		--print( "Unique touch ID: "..tostring(event.id) )
		if (event.phase == "began") then
			moveLeft = true
			--print("l t")
		elseif ( (event.phase == "ended" or event.phase == "cancelled") and moveLeft == true ) then
			wasMoving = true
        	moveLeft = false
        	player:setIdle()
        	--print("l f")
    	end
    	return true
	end

	local function handleRightBtn(event)
		--print("r e")
		--print( "Unique touch ID: "..tostring(event.id) )
		if (event.phase == "began") then
			moveRight = true
			--print("r t")
		elseif ( (event.phase == "ended" or event.phase == "cancelled") and moveRight == true ) then
			wasMoving = true
        	moveRight = false
        	player:setIdle()
        	--print("r f")
    	end
    	return true
	end


	local function handleJumpBtn(event)
		--print("j e")
		if ( jump == false and event.phase == "began") then
			--print("j t")
			jump = true
		elseif(event.phase == "ended" or event.phase == "moved") then
			jump = false
		end
	end

	local function handleAtkBtn(event)
		if(event.phase == "began" and attack == false) then
			attack  = true
		elseif(event.phase == "ended" or event.phase == "moved") then
			attack = false
			wasAtking = true
		end
	end




	visualButton.leftBtn = widget.newButton{
		defaultFile = "images/buttons/left arrow.png",
		overFile = "images/buttons/left arrow.png",
		width = 170, height = 170,
		x = 90, y = _H - 100,
	}

	controller:insert(visualButton.leftBtn)
	visualButton.leftBtn:addEventListener("touch", handleLeftBtn)

	visualButton.rightBtn = widget.newButton{
		defaultFile = "images/buttons/right arrow.png",
		overFile = "images/buttons/right arrow.png",
		width = 170, height = 170,
		x = visualButton.leftBtn.x + 180, y = _H - 100
		
	}
	visualButton.rightBtn:addEventListener("touch", handleRightBtn)
	controller:insert(visualButton.rightBtn)


	visualButton.jumpBtn = widget.newButton{
		defaultFile = "images/buttons/up arrow.png",
		overFile = "images/buttons/up arrow.png",
		width = 80, height = 80,
		x = _W - 200, y = _H - 60,
		onEvent = handleJumpBtn
	}
	controller:insert(visualButton.jumpBtn)

	visualButton.atkBtn = widget.newButton{
		defaultFile = "images/buttons/atk.png",
		overFile = "images/buttons/atk.png",
		width = 170, height = 170,
		x = _W - 98, y = _H - 100,
		onEvent = handleAtkBtn
	}
	controller:insert(visualButton.atkBtn)

	-- visualButton.weapon = widget.newButton{
	-- 	defaultFile = nil,
	-- 	overFile = nil,
	-- 	width = 70, height = 70,
	-- 	x = _W-200, y = _H - 150,
	-- 	onEvent = function ()
	-- 		player:dropWeapon()
	-- 	end
	-- }
	-- controller:insert(visualButton.weapon)

	visualButton.itemSLot = widget.newButton{
		defaultFile = "images/buttons/circle.png",
		overFile = "images/buttons/circle.png",
		width = 80, height = 80,
		x = _W-200, y = _H - 150,
		onEvent = function ()
			player:dropWeapon()
		end
	}


	-- visualButton.itemSLot = display.newImageRect("images/buttons/circle.png",visualButton.jumpBtn.width,visualButton.jumpBtn.height)
	-- visualButton.itemSLot.x = visualButton.jumpBtn.x
	-- visualButton.itemSLot.y = visualButton.jumpBtn.y - 90

	controller:insert(visualButton.itemSLot)


	chatBox.chatBtn = widget.newButton{
		label="Pause to Chat",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		font = "introrustg-base2line.otf",
		width=160, height=40,
		x = _CX, y = _H - 30,
		onRelease = function()
			scene:Pause()
			isChatBtnOn = false
			updateAndDisplayChatBox()
			player:sendPause()
		end

	}
	chatBox.chatBtn.isVisible = true

	chatBox.resume = widget.newButton{
		label="Resume",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=150, height=30,
		x = _W - 200, y = _H - 30,
		onRelease = function()
			scene:continue()
			isChatBtnOn = true
			updateAndDisplayChatBox()
			player:continue()
		end

	}
	chatBox.resume.isVisible = false

	function controller:setPause()
		isChatBtnOn = false
		updateAndDisplayChatBox()
	end

	function controller:continue()
		isChatBtnOn = true
		updateAndDisplayChatBox()
	end



	
	local function textListener( event )
    	if ( event.phase == "began" ) then
        -- User begins editing "defaultField"
    	elseif (event.phase == "submitted" ) then
	        -- Output resulting text from "defaultField"
	        local text = event.target.text
	        --print(text)
	        player:setMessage(text)
	        chatBox.textField.text = ""
	    --[[elseif ( event.phase == "editing" ) then
	        print( event.newCharacters )
	        print( event.oldText )
	        print( event.startPosition )
	        print( event.text )		]]
	    end
	end
	-- Create text field
	chatBox.textField = native.newTextField(_W + 500 , _H + 500, 220, 30 )
	chatBox.textField:addEventListener( "userInput", textListener )

	controller:insert(chatBox.chatBtn)
	controller:insert(chatBox.textField)
	controller:insert(chatBox.resume)

	scene.view:insert(controller)







	return controller
end

return _M

