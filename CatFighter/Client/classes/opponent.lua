-- Opponent Class
-- Opponent object in the game and contains functions to be called
-- by the game class.
-- Opponent will be controlled as a 'reflection' of the other
-- client's movement over the server. 

local physics = require('physics')
local relayout = require('libs.relayout')
local newTest = require('libs.opponentTest').newTest

local _M = {}

local _W, _H, _CX, _CY = relayout._W, relayout._H, relayout._CX, relayout._CY

-- Configurations for image sheet
local options = {
   	width = 64,
   	height = 64,
   	numFrames = 256
}

-- image sheet of opponent sprite
local imageSheet1 = graphics.newImageSheet( "images/cat_sprite/cat_full.png", options )
local imageSheet2 = graphics.newImageSheet( "images/cat_sprite/cat_full_blue.png", options )

-- Sequence data for opponent sprite
local sequenceData =
{
	-- idle mode frames
	{
    name="idle",
    start=1,
    count=4,
    time= 500,
    loopCount = 0,   -- Optional ; default is 0 (loop indefinitely)
    loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
	},
	-- walking frames
	{
    name="walking",
    start=17,
    count=8,
    time= 500,
    loopCount = 0,   -- Optional ; default is 0 (loop indefinitely)
    loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
	},
	-- jumping frames
	{
    name="jumping",
    start=33,
    count=8,
    time= 800,
    loopCount = 1,   -- Optional ; default is 0 (loop indefinitely)
    loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
	},
	{
	name = "attack1",
	start = 145,
	count = 6,
	time = 500,
	loopCount = 1,
	loopDirection = "forward"
	},
	{
	name = "attack2",
	start = 151,
	count = 4,
	time = 500,
	loopCount = 1,
	loopDirection = "forward"
	},
	{
	name = "attack3",
	start = 161,
	count = 5,
	time = 500,
	loopCount = 1,
	loopDirection = "forward"
	},
	{
	name = "attack4",
	start = 168,
	count = 5,
	time = 500,
	loopCount = 1,
	loopDirection = "forward"
	},
	{
	name = "attack5",
	start = 178,
	count = 5,
	time = 500,
	loopCount = 1,
	loopDirection = "forward"
	},
	{
	name = "weaponAtk",
	start = 241,
	count = 6,
	time = 600,
	loopCount = 1,
	loopDirection = "forward"
	},
	{
	name = "die",
	start = 65,
	count = 7,
	time = 1000,
	loopCount = 1,
	loopDirection = "forward"
	}
}

function _M.newOpponent(params)
	local opponent
	print("new player posi is " .. params.posi)
	if(params.posi == "1") then
		opponent = display.newSprite( imageSheet1, sequenceData )
		print(params.posi)
	else
		opponent = display.newSprite(imageSheet2, sequenceData)
		print(params.posi)

	end

	-- Initial direction in which the sprite is facing based on params
	opponent.direction = params.dir
	if (opponent.direction == "left") then
    	opponent:scale( -params.scale, params.scale)
    else 
    	opponent:scale( params.scale, params.scale)
   	end

	opponent.anchorY = 1
	opponent.x, opponent.y = params.x, params.y

	-- Check if opponent is allowed to jump
	opponent.canJump = 0

	-- Check if opponent is in contact with the ground
	opponent.inAir = false

	-- Check if opponent is moving or idle
	opponent.moving = false

	-- Sprite is set to idle animation when not moving
	opponent:setSequence( "idle" )
	opponent:play()

	opponent.health = 100


	-- indicate currently taking attack action
	opponent.attack = false

	opponent.weapon = false

	local test = newTest(opponent)

	-- rectangular physics area for the sprite
	local rectangleShape = { -15,10, 15,10, 15,-65, -15,-65}

	-- Describing parameters for physics body
	local bodyParams = {shape = rectangleShape, density = 20000, friction = 0, bounce = 0}
	physics.addBody(opponent, 'dynamic', bodyParams)
	opponent.isFixedRotation = true


	--Indicate whether the opponent in collide with player
	local touchingEnemy = false
	local player = nil
	-- Collision function which describes the collision of the opponent
	-- with the ground. 
	-- Output: jumping boolean and inAir boolean
	function charCollide( self,event )
   		if ( event.selfElement == 1 and event.other.objType == "ground" ) then
    		if ( event.phase == "began") then
        		self.canJump = self.canJump+1
        		if (opponent.inAir == true) then
        			opponent:setSequence("idle")
					opponent:play()
				end
				opponent.inAir = false
      		elseif ( event.phase == "ended" ) then
         		self.canJump = self.canJump-1
     		end
   		end
   		if(event.other.objType == "player") then
   			if(event.phase == "began") then
   				--print("t p")
   				touchingEnemy = true
   				player = event.other
   			elseif(event.phase == "ended") then
   				--print("t p f")
   				touchingEnemy = false
   				player = nil
   			end
   		end
	end
	opponent.collision = charCollide 
	opponent:addEventListener("collision", opponent)

	local didDamage = false
	function handleDamage(event)
		if(touchingEnemy == true and didDamage == false) then
			if(opponent.sequence == "attack1") then 
				opponent:doDamage(player,5)
				didDamage = true
			elseif(opponent.sequence == "attack2") then
				opponent:doDamage(player,5)
				didDamage = true
			elseif(opponent.sequence == "attack3") then
				opponent:doDamage(player,5)
				didDamage = true
			elseif(opponent.sequence == "attack4") then
				opponent:doDamage(player,5)
				didDamage = true
			elseif(opponent.sequence == "attack5") then
				opponent:doDamage(player,5)
				didDamage = true
			end
		end
	end	

	Runtime:addEventListener( "enterFrame", handleDamage )

	--let player do damage to opponent
	function opponent:doDamage(player,damage)
		timer:performWithDelay(50,player:getDamage(damage))
	end

	function opponent:getDamage(damage)
		if(opponent.health - damage < 0) then
			opponent.health = 0
			if(opponent.sequence ~= "die") then
				opponent:setSequence("die")
				opponent:play()
			end
		else
			opponent.health = opponent.health - damage
		end
	end

	function opponent:getHealth()
		return opponent.health
	end


	function opponent:setHealth(hp)
		if(hp > 100) then
			opponent.health = 100
		elseif(hp < 0) then
			opponent.health = 0
			if(opponent.sequence ~= "die") then
				opponent:setSequence("die")
				opponent.isDead = true
				opponent:play()
			end
		else
			opponent.health = hp
		end
	end


		-- The sprite listener for setting idle after attack
	function sprite_listener(event)
		local current_sprite = event.target
		if (current_sprite.sequence == "attack1" and event.phase == "ended") then
			--print("c atk")
			--print(attack)
			if (opponent.attack == true) then
				current_sprite:setSequence("attack2")
				current_sprite:play()
				didDamage = false
				--print("opponenting atk2")
			else
				current_sprite:setSequence("idle")
				current_sprite:play()
			end
		elseif (current_sprite.sequence == "attack2" and event.phase == "ended") then
			if (opponent.attack == true) then
				current_sprite:setSequence("attack3")
				current_sprite:play()
				didDamage = false
				--print("opponenting atk2")
			else
				current_sprite:setSequence("idle")
				current_sprite:play()
			end
		elseif (current_sprite.sequence == "attack3" and event.phase == "ended") then
			if (opponent.attack == true) then
				current_sprite:setSequence("attack4")
				current_sprite:play()
				didDamage = false
				--print("opponenting atk2")
			else
				current_sprite:setSequence("idle")
				current_sprite:play()
			end
		elseif (current_sprite.sequence == "attack4" and event.phase == "ended") then
			if (opponent.attack == true) then
				current_sprite:setSequence("attack5")
				current_sprite:play()
				didDamage = false
				--print("opponenting atk2")
			else
				current_sprite:setSequence("idle")
				current_sprite:play()
			end
		elseif (current_sprite.sequence == "attack5" and event.phase == "ended") then
			current_sprite:setSequence("idle")
			current_sprite:play()
		elseif(current_sprite.sequence == "weaponAtk" and event.phase == "ended") then
			current_sprite:setSequence("idle")
			current_sprite:play()
		end

	end

	opponent:addEventListener("sprite",sprite_listener)

		-- function to attack if requested
	function opponent:setAttackTrue()
		opponent.attack = true
		if(opponent.sequence == "idle" or opponent.sequence == "walking") then
			if(opponent.weapon ~= false) then
				opponent:setSequence("weaponAtk")
			else
				opponent:setSequence("attack1")
			end

			opponent:play()
		end

	end

	-- function to attack if requested
	function opponent:setAttackFalse()
		opponent.attack = false

	end


	-- Jump function 
	function opponent:jump()
		opponent.inAir = true
		opponent.x = opponent.x + 5
		opponent:setLinearVelocity(0, -1300)
		opponent:setSequence("jumping")
		opponent:play()
		test:OpponentJump()
	end

	-- Function to move right if requested
	function opponent:moveRight()
		local x1 = opponent.x
			if (opponent.direction == "left") then
				opponent.direction = "right"
				opponent.xScale = 2.5
			end
			opponent.x = opponent.x + 5
			if (opponent.sequence == "idle") then
				opponent:setSequence("walking")
				opponent:play()
			end
		local x2 = opponent.x
		test:OpponentMove(x1,x2,"right")
	end

	-- Function to move left if requested
	function opponent:moveLeft()
		local x1 = opponent.x
			if (opponent.direction == "right") then
				opponent.direction = "left"
				opponent.xScale = -2.5
			end
			opponent.x = opponent.x - 5
			if (opponent.sequence == "idle") then
				opponent:setSequence("walking")
				opponent:play()
			end
		local x2 = opponent.x
		test:OpponentMove(x1,x2,"left")
	end

	-- Sets opponent sprite to idle mode if not moving
	function opponent:setIdle()
		opponent:setSequence("idle")
		opponent:play()
	end


	-- Set the weapon of player when player picks it up
	function opponent:pickUpWeapon()
		opponent.weapon = true
	end

	function opponent:dropWeapon()
		opponent.weapon = false
	end

	return opponent
end

return _M