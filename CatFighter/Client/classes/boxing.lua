-- Boxing Class
-- Boxing gloves object in the game and contains functions to be called
-- by the game class.

local physics = require('physics')
local relayout = require('libs.relayout')


local _M = {}

local _W, _H, _CX, _CY = relayout._W, relayout._H, relayout._CX, relayout._CY

function _M.newBoxing(params)

	local boxing = display.newImage( "images/weapon_sprite/gloves.png", params.boxingx, 0)
	boxing:scale(0.1, 0.1)

	boxing.x = params.boxingx
	boxing.y = params.boxingy

	local pause = false

	boxing.count = 0

	print("new count", boxing.count)
	print("boxing x", boxing.x)
	print("boxing y", boxing.y)

	function boxingTimer(event)
		-- if the boxing gloves has been existing on the screen for more than 600 frames and 
		-- less than 750 frames, make it flash to remind the players that the boxing gloves
		-- are about to disappear
		if(pause ~= true) then
   			if (boxing.count>=600 and boxing.count<750) then
   				if (boxing.count%10==0) then
					boxing.isVisible = false
   				else
   					boxing.isVisible = true
   				end
				boxing.count = boxing.count + 1
   				-- make the boxing gloves invisible after 750 frames since they appeared
   			elseif (boxing.count >= 750) then
   				Runtime:removeEventListener("enterFrame", boxingTimer)
   				boxing.isVisible = false
   				boxing.count = -1
   			elseif (boxing.count>=0 and boxing.count<600) then
   				if (boxing.y < (display.contentHeight - 250)) then
	   				boxing.y = boxing.y + 8
	  			end
	  			boxing.count = boxing.count + 1
	  		end
   		end
	end


	function boxing:Pause()
		pause = true
	end

	function boxing:Continue()
		pause = false
	end

	Runtime:addEventListener("enterFrame", boxingTimer)

	return boxing
end

return _M