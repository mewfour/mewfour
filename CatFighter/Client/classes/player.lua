-- Player Class
-- Contains the main player in the game. 
-- Functions included movement configurations
-- Will send message to udp entered in params

local physics = require('physics')
local relayout = require('libs.relayout')
local json = require('json')
local encoder = require('libs.easyEncoder')
local newTest = require('libs.playerTest').newTest
local newItemTest = require('libs.itemTest').newItemTest

-- Path for recording data file
local path =  system.pathForFile("self_data.txt", system.DocumentsDirectory)

local recording = true
local frame_no = 0
local player_win = false

local _M = {}

local _W, _H, _CX, _CY = relayout._W, relayout._H, relayout._CX, relayout._CY

-- Configurations for image sheet
local options = {
   	width = 64,
   	height = 64,
   	numFrames = 256
}

-- image sheet of player sprite
local imageSheet1 = graphics.newImageSheet( "images/cat_sprite/cat_full.png", options )
local imageSheet2 = graphics.newImageSheet( "images/cat_sprite/cat_full_blue.png", options )


-- Sequence data for player sprite
local sequenceData =
{
	-- idle mode frames
	{
    name="idle",
    start=1,
    count=4,
    time= 500,
    loopCount = 0,   -- Optional ; default is 0 (loop indefinitely)
    loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
	},
	-- walking frames
	{
    name="walking",
    start=17,
    count=8,
    time= 500,
    loopCount = 0,   -- Optional ; default is 0 (loop indefinitely)
    loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
	},
	-- jumping frames
	{
    name="jumping",
    start=33,
    count=8,
    time= 800,
    loopCount = 1,   -- Optional ; default is 0 (loop indefinitely)
    loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
	},
	{
	name = "attack1",
	start = 145,
	count = 6,
	time = 500,
	loopCount = 1,
	loopDirection = "forward"
	},
	{
	name = "attack2",
	start = 151,
	count = 4,
	time = 500,
	loopCount = 1,
	loopDirection = "forward"
	},
	{
	name = "attack3",
	start = 161,
	count = 5,
	time = 500,
	loopCount = 1,
	loopDirection = "forward"
	},
	{
	name = "attack4",
	start = 168,
	count = 5,
	time = 500,
	loopCount = 1,
	loopDirection = "forward"
	},
	{
	name = "attack5",
	start = 178,
	count = 5,
	time = 500,
	loopCount = 1,
	loopDirection = "forward"
	},
	{
	name = "weaponAtk",
	start = 241,
	count = 6,
	time = 600,
	loopCount = 1,
	loopDirection = "forward"
	},
	{
	name = "die",
	start = 65,
	count = 7,
	time = 1000,
	loopCount = 1,
	loopDirection = "forward"
	}
}

function _M.newPlayer(params)
	local player
	if(params.posi == "1") then
		player = display.newSprite( imageSheet1, sequenceData )
	else
		player = display.newSprite(imageSheet2, sequenceData)
	end

	-- Initial direction in which the sprite is facing based on params
	player.direction = params.dir
	if (player.direction == "right") then
    	player:scale( params.scale, params.scale)
    else
    	player:scale( -params.scale, params.scale)
    end

	player.anchorY = 1
	player.x, player.y = params.x, params.y

	-- Check if player is allowed to jump
	player.canJump = 0

	-- Check if player is in contact with the ground
	player.inAir = false

	-- Check if player jumped
	player.jumped = false

	-- player is set to idle animation when not moving
	player:setSequence( "idle" )
	player:play()

	-- Check which key is currently active
	player.keyRight = false
	player.keyLeft = false

	-- Check if player was moving previously before idling
	player.wasMoving = false

	player.controlleft = false
	player.controlright = false

	--Initial health of player
	player.health = 100
	player.isDead = false


	-- indicate currently taking attack action
	player.attack = false
	player.wasAtking = false


	-- The weapon the player holds
	player.haveWeapon = false
	player.weapon = nil

	local test = newTest(player)
	local itemTest = newItemTest(player)

	-- rectangular physics area for the sprite
	local rectangleShape = { -15,10, 15,10, 15,-65, -15,-65}

	-- Describing parameters for physics body
	local bodyParams = {shape = rectangleShape, density = 20000, friction = 0, bounce = 0}
	physics.addBody(player, 'dynamic', bodyParams)
	player.isFixedRotation = true


	--Indicate whether the player in collide with opponent
	local touchingEnemy = false
	local opponent = nil

	-- Collision function which describes the collision of the opponent
	-- with the ground. 
	-- Output: jumping boolean and inAir boolean
	function charCollide( self,event )
   		if ( event.selfElement == 1 and event.other.objType == "ground" ) then
    		if ( event.phase == "began") then
        		self.canJump = self.canJump+1
        		if (player.inAir == true) then
        			player:setSequence("idle")
					player:play()
				end
				player.inAir = false
      		elseif ( event.phase == "ended" ) then
         		self.canJump = self.canJump-1
     		end
   		end
   		if(event.other.objType == "opponent") then
   			if(event.phase == "began") then
   				--print("t e")
   				touchingEnemy = true
   				opponent = event.other
   			elseif(event.phase == "ended") then
   				--print("t e f")
   				touchingEnemy = false
   				opponent = nil
   			end
   		end
	end
	player.collision = charCollide 
	player:addEventListener("collision", player)


	-- Record player data to a local file
	local function recordData(data)
		if (recording) then
			file = io.open(path, "a")
			file:write("frame:" .. frame_no .. "\n" .. data)
			file:close()
	    end
	end

	-- Increace frame number 
	function addFrame(event)
		frame_no = frame_no + 1
	end


	--indicate this attack has made damage to the opponent
	local didDamage = false
	function handleDamage(event)
		if(touchingEnemy == true and didDamage == false) then
			if(player.sequence == "attack1") then 
				player:doDamage(opponent,5)
				didDamage = true
			elseif(player.sequence == "attack2") then
				player:doDamage(opponent,5)
				didDamage = true
			elseif(player.sequence == "attack3") then
				player:doDamage(opponent,5)
				didDamage = true
			elseif(player.sequence == "attack4") then
				player:doDamage(opponent,5)
				didDamage = true
			elseif(player.sequence == "attack5") then
				player:doDamage(opponent,5)
				didDamage = true
			elseif(player.sequence == "weaponAtk") then
				player:doDamage(opponent, 15)
				didDamage = true
			end

		end
	end

	Runtime:addEventListener( "enterFrame", handleDamage )



	--let player do damage to opponent
	local opHp = 100
	function player:doDamage(opponent,damage)
		local hp1 = opponent:getHealth()

		timer:performWithDelay(100, opponent:getDamage(damage))
		opHp = opponent:getHealth()

		local hp2 = opponent:getHealth()

		--test:PlayerMakeDamage(hp1,hp2)


	end

	--let player receive damage 
	function player:getDamage(damage)
		--player.health = player.health - damage
		print("pl")
		print(player.health)
	end

	--  get player's health
	function player:getHealth()
		return player.health

	end

	function player:setHealth(hp)
		if(hp > 100) then
			player.health = 100
		elseif(hp < 0) then
			player.health = 0
			if(player.sequence ~= "die") then
				player:setSequence("die")
				player.isDead = true
				player:play()
			end
		else
			player.health = hp
		end
	end



	-- The sprite listener for setting idle after attack
	function sprite_listener(event)
		local current_sprite = event.target
		if (current_sprite.sequence == "attack1" and event.phase == "ended") then
			if (player.attack == true) then
				current_sprite:setSequence("attack2")
				current_sprite:play()
				didDamage = false
				--print("playering atk2")
			else
				current_sprite:setSequence("idle")
				current_sprite:play()
			end
		elseif (current_sprite.sequence == "attack2" and event.phase == "ended") then
			if (player.attack == true) then
				current_sprite:setSequence("attack3")
				current_sprite:play()
				didDamage = false
				--print("playering atk2")
			else
				current_sprite:setSequence("idle")
				current_sprite:play()
			end
		elseif (current_sprite.sequence == "attack3" and event.phase == "ended") then
			if (player.attack == true) then
				current_sprite:setSequence("attack4")
				current_sprite:play()
				didDamage = false
				--print("playering atk2")
			else
				current_sprite:setSequence("idle")
				current_sprite:play()
			end
		elseif (current_sprite.sequence == "attack4" and event.phase == "ended") then
			if (player.attack == true) then
				current_sprite:setSequence("attack5")
				current_sprite:play()
				didDamage = false
				--print("playering atk2")
			else
				current_sprite:setSequence("idle")
				current_sprite:play()
			end
		elseif (current_sprite.sequence == "attack5" and event.phase == "ended") then
			current_sprite:setSequence("idle")
			current_sprite:play()
		elseif(current_sprite.sequence == "weaponAtk" and event.phase == "ended") then
			current_sprite:setSequence("idle")
			current_sprite:play()
		end

	end

	player:addEventListener("sprite",sprite_listener)


		--Contoller to control the player
	function player:controlRight(event)
		local x1 = player.x
		player.controlright = true
		if (player.x < _W - 32 and player.isDead == false) then
			player.x = player.x + 5
			--print("controlRight")
			if (player.direction == "left") then
				player.direction = "right"
				player.xScale = 2.5
			end
			if (player.sequence == "idle") then
				player:setSequence("walking")
				player:play()
			end
		end
		local x2 = player.x
		test:PlayerMove(x1,x2,"right")

	end

	-- function to move left if requested
	function player:controlLeft(event)
		player.controlleft = true
		local x1 = player.x
		if (player.x > 32 and player.isDead == false) then
			player.x = player.x - 5
			--print("controlLeft")
			if (player.direction == "right") then
				player.direction = "left"
				player.xScale = -2.5
			end
			if (player.sequence == "idle") then
				player:setSequence("walking")
				player:play()
			end
		end
		local x2 = player.x
		test:PlayerMove(x1,x2,"left")
		--test:PlayerMove(x1,x2,"left")
	end

	function player:stop(event)
		player.controlleft = false
		player.controlright = false
		player.wasMoving = true

	end

	function player:controlJump(event)
		if (player.canJump > 0 and player.isDead == false) then
			player.jumped = true
			player.inAir = true
			player:setLinearVelocity(0, -1300)
			player:setSequence("jumping")
			player:play()
			test:PlayerJump()
		end

	end

	-- Set the weapon of player when player picks it up
	player.weaponImg = display.newImage("images/weapon_sprite/gloves.png",_W*4, _H*4)
	player.weaponImg:scale(0.1,0.1)
	function player:pickUpWeapon(wp)
		player.haveWeapon = true
		player.weapon = wp
		player.weaponImg.x = _W - 200
		player.weaponImg.y = _H - 150 

		itemTest:PlayerPickedWeapon(player.weapon)
	end

	player.weaponDroped = false
	function player:dropWeapon()
		player.haveWeapon = false
		player.weapon = nil
		player.weaponImg.x = _W *4
		player.weaponImg.y = _H *4
		player.weaponDroped = true

		itemTest:PlayerDropedWeapon(player.weapon)

	end


	-- function to attack if requested
	function player:setAttackTrue(event)
		player.attack = true
		
		if((player.sequence == "idle" or player.sequence == "walking") and player.isDead == false) then
			if(player.haveWeapon == true) then
				player:setSequence("weaponAtk")
			else
				player:setSequence("attack1")
			end
			player:play()
			didDamage = false
		end

	end

	--cancell the attack state
	function player:setAttackFalse(event)
		player.attack = false
		player.wasAtking = true

	end

	-- set player back to idle
	function player:setIdle(event)
		player:setSequence("idle")
		player:play()
	end


	local textController = params.tc
	player.message = nil
	player.sendMessage = false
	function player:setMessage(string)
		player.message = string
		player.sendMessage = true
		textController:updateText("player",string)

	end


	player.needPause = false
	function player:sendPause()
		player.needPause = true
	end

	player.needContinue = false
	function player:continue()
		player.needContinue = true
	end





	-- Encodes message and send it via udp
	function send_message(t)
		encoded_message = encoder.encode(t)
    	udp:send(encoded_message)

    	-- Record encoded message
    	recordData(encoded_message)
	end

	-- send message annoucing the destruction of an item
	function player:deleteItemMsg(i)
		if (i==1) then
			message = {direction = "none", 
							jump = "none", 
							x = -1,
							y = -1, 
							send_coords = "false",
							attack = "false", 
							opHp = opHp, 
							message = "", 
							sendMessage = "false", 
							available = "true", 
							item = "delete",  
							deleteItem = "1"
							}
		elseif (i==2) then
			message = {direction = "none", 
							jump = "none", 
							x = -1,
							y = -1, 
							send_coords = "false",
							attack = "false", 
							opHp = opHp, 
							message = "", 
							sendMessage = "false", 
							available = "true", 
							item = "delete",  
							deleteItem = "2"
							}
		end
		send_message(message)
	end


	local periodic_send_counter = 50
	-- Sends message if updated (per frame)
	-- message structure:
	-- direction = direction in which player is moving (if active)
	-- jump = player jumped
	-- send_coords = boolean: sends coordinates if player is moving and not jumping
	-- available = boolean: sends message if there is an update
	function periodic_send(event)
		--local player_coordinates = {x = player.x, y = player.y}

		-- message structure
		local message = {direction = "none", 
					jump = "none", 
					send_coords = "true",
					x = player.x,
					y = player.y, 
					
					attack = "false",
					opHp = opHp,
					message = player.message,
					sendMessage = "false",
					available = "false", 
					item = "none",
					deleteItem = "none"
					}

		periodic_send_counter = periodic_send_counter - 1
		if (periodic_send_counter<=0) then
			udp:send("hello")
			periodic_send_counter = 50
		end

		if (opHp <= 0 and player_win == false) then
			player_win = true
			print("send opponentlose")
			timer.performWithDelay(1000,udp:send ("opponentlose"))
			
		end

		-- update direction
		if (player.controlright == true) then
			message.direction = "right"
			message.available = "true"
		elseif (player.controlleft == true) then
			message.direction = "left"
			message.available = "true"
		end

		-- update jump
		if (player.jumped == true) then
			message.jump = "jump"
			player.jumped = false
			message.available = "true"
		end

		-- update send_coords boolean
		if (player.inAir == true) then
			send_coords = "false"
		end

		-- update attack commend
		if(player.attack == true) then
			message.attack = "true"
			message.available = "true"
		end

		if(player.sendMessage == true) then
			--print("sending message")
			message.sendMessage = "true"
			message.available = "true"
			player.sendMessage = false
		end

		if(player.weaponDroped == true) then
			message.deleteItem = "drop"
			message.available = "true"
			player.weaponDroped = false
		end
		


		-- activates idle mode (if player was moving and now stopped)
		if (player.wasMoving == true and player.inAir == false) then
			message.available = "true"
			message.direction = "idle"
			player.wasMoving = false
		end
		if(player.wasAtking == true) then
			message.direction = "idle"
			message.available = "true"
			player.wasAtking = false
		end

		-- send request to pause the game for chat
		if(player.needPause == true) then
			message.direction = "pause"
			message.available = "true"
			player.needPause = false
			player.weaponImg.isVisible = false
		end

		-- send request to resume the game from chat
		if(player.needContinue == true) then
			message.direction = "continue"
			message.available = "true"
			player.needContinue = false
			player.weaponImg.isVisible = true
		end

		-- update available, send message.
		if (message.available == "true") then
			send_message(message)
		end
	end
	Runtime:addEventListener( "enterFrame", periodic_send)
	Runtime:addEventListener( "enterFrame", addFrame)

	params.g:insert(player)
	params.g:insert(player.weaponImg)


	-- runtime remover
	function player:removeRuntime()
		Runtime:removeEventListener( "enterFrame", periodic_send)
		Runtime:removeEventListener( "enterFrame", addFrame)
	end

	return player
end

return _M
