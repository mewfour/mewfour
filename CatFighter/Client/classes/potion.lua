-- Potion Class
-- Potion object in the game and contains functions to be called
-- by the game class.

local physics = require('physics')
local relayout = require('libs.relayout')


local _M = {}

local _W, _H, _CX, _CY = relayout._W, relayout._H, relayout._CX, relayout._CY

function _M.newPotion(params)


	local potion = display.newImage( "images/weapon_sprite/potion.png", params.potionx, 0)

	potion.x = params.potionx
	potion.y = params.potiony

	local pause = false

	potion.count = 0

	print("new count", potion.count)
	print("potion x", potion.x)
	print("potion y", potion.y)

	function potionTimer(event)
		-- if the potion has been existing on the screen for more than 600 frames and less than 
		-- 750 frames, make it flash to remind the players that the potion is about to disappear
		if(pause ~= true) then
   			if (potion.count>=600 and potion.count<750) then
   				if (potion.count%10==0) then
					potion.isVisible = false
   				else
   					potion.isVisible = true
   				end
				potion.count = potion.count + 1
   				-- make the potion invisible after 750 frames since the potion appeared
   			elseif (potion.count >= 750) then
   				Runtime:removeEventListener("enterFrame", potionTimer)
   				potion.isVisible = false
   				potion.count = -1
   			elseif (potion.count>=0 and potion.count<600) then
   				if (potion.y < (display.contentHeight - 250)) then
	   				potion.y = potion.y + 8
	  			end
	  			potion.count = potion.count + 1
	  		end
   		end
	end


	function potion:Pause()
		pause = true
	end

	function potion:Continue()
		pause = false
	end

	Runtime:addEventListener("enterFrame", potionTimer)

	return potion
end

return _M