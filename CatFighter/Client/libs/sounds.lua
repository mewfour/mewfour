-- Sounds library
-- Manager for the sound and music files.
-- Automatically loads files and keeps a track of them.
-- Use playSteam() for music files and play() for short SFX files.

local _M = {}

_M.isSoundOn = true
_M.isMusicOn = true

local sounds = {
}

-- Reserve two channels for streams and switch between them with a nice fade out / fade in transition
local audioChannel, otherAudioChannel, currentStreamSound = 1, 2

function _M.playStream(sound, force)
end
audio.reserveChannels(2)

-- Keep all loaded sounds here
local loadedSounds = {}

local function loadSound(sound)
end

function _M.play(sound, params)
end

function _M.stop()
end

return _M
