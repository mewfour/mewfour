--Unit testing package for player

local relayout = require('libs.relayout')

local _W, _H, _CX, _CY = relayout._W, relayout._H, relayout._CX, relayout._CY
local _M = {}

function _M.newTest(player)
	local test = {
	success = 0,
	fail = 0,
	total = 0
	}

	test.moveTest = false
	test.jumpTest = false
	test.atkTest = false
	
	function test:PlayerMove(x1,x2,dir)
		if(test.moveTest ~= true) then
			test.total = test.total +1 
			assert(player, "Player is nil")
			if(dir == "right" and x1 < _W-32) then
				assert(x1<x2, "Moving wrong dir")
			elseif(dir == "left" and x1 > 32) then
				assert(x1>x2, "Moving wrong dir")
			end
		
			test.success = test.success + 1
			test.moveTest = true
			print("Test : ")
			print(test.total)
			print("Passed: ")
			print(test.success)
		end
		
	end

	function test:PlayerJump()
		if(test.jumpTest ~= true) then
			test.total = test.total +1 
			local vx,vy = player:getLinearVelocity()
			assert(vy<0, "player not jumping")
			test.success = test.success + 1
			test.jumpTest = true
			print("Test : ")
			print(test.total)
			print("Passed: ")
			print(test.success)
		end
		
	end



	function test:PlayerMakeDamage(opHp1, opHp2)
		if(test.atkTest ~= true) then
			test.total = test.total +1
			if(opHp1 > 0 ) then
				assert(opHp2<opHp1, "Player not making damage to opponent")
			end
			test.success = test.success + 1
			test.atkTest = true

			print("Test : ")
			print(test.total)
			print("Passed: ")
			print(test.success)
		end
	end


	return test
end

return _M