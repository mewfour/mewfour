local relayout = require('libs.relayout')

local _M = {}

function _M.newTextController(group)
	local _W,_H,_CX,_CY = relayout._W, relayout._H, relayout._CX, relayout._CY
	local width, height = 220,30

	local textGroup = display.newGroup()
	local textController = {}


	textController.line1 = display.newText("", _CX, _H - 70,width,height,native.systemFont, 25)
	textController.line2 = display.newText("", _CX, _H - 110,width,height,native.systemFont, 25)
	textController.line3 = display.newText("", _CX, _H - 150,width,height,native.systemFont, 25)
	textController.line1.color = "white"
	textController.line2.color = "white"
	textController.line3.color = "white"


	local function setToText(text1,text2)
		text1.text = text2.text
		local color = text2.color
		text1.color = color
		if(color == "blue") then
			text1:setFillColor(0,255,255)
		elseif(color == "red") then
			text1:setFillColor(240,0,0)
		end
	end

	function textController:updateText(player,string)
		--print(string)
		setToText(textController.line3, textController.line2)
		setToText(textController.line2,textController.line1)
		textController.line1.text = string
		if(player == "opponent") then
			textController.line1.color = "red"
			textController.line1:setFillColor(240,0,0)
		elseif(player == "player") then
			textController.line1.color = "blue"
			textController.line1:setFillColor(0,255,255)
		end

	end

	textGroup:insert(textController.line1)
	textGroup:insert(textController.line2)
	textGroup:insert(textController.line3)

	group:insert(textGroup)


	return textController


end

return _M