--Unit testing package for item

local relayout = require('libs.relayout')

local _W, _H, _CX, _CY = relayout._W, relayout._H, relayout._CX, relayout._CY
local _M = {}

function _M.newItemTest(player)
	local test = {
	success = 0,
	fail = 0,
	total = 0
	}

	test.healTest = false
	test.pickTest = false
	test.dropTest = false
	function test:ItemGenerate(item)
		assert(item,"item is nil")
	end

	function test:ItemHeal(hp1,hp2)
		test.total = test.total +1
		if(test.healTest == false) then
			if(hp1 < 100) then
				assert(hp2 > hp1, "not healing")
			end

			test.success = test.success +1
			test.healTest = true

			print("Test : ")
			print(test.total)
			print("Passed: ")
			print(test.success)
		end

	end

	function test:PlayerPickedWeapon(weapon)
		if(test.pickTest ~= true) then
			test.total = test.total +1

			assert(weapon ~= nil, "player not picking weapon")
			
			test.success = test.success +1
			test.pickTest = true

			print("Test : ")
			print(test.total)
			print("Passed: ")
			print(test.success)
		end
	end	

	function test:PlayerDropedWeapon(weapon)
		if(test.dropTest ~= true) then
			test.total = test.total +1

			assert(weapon == nil, "player not droping weapon")
			
			test.success = test.success +1
			test.dropTest = true

			print("Test : ")
			print(test.total)
			print("Passed: ")
			print(test.success)
		end
	end



	return test
end

return _M