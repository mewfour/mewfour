local composer = require( "composer" )
local widget = require "widget"
local s = require("socket")
local relayout = require('libs.relayout')
local sounds = require('libs.sounds')

local scene = composer.newScene()

local replayBtn

function scene:create(event)
	local group = self.view
	local _W, _CX, _CY = relayout._W, relayout._CX, relayout._CY

	local background = display.newImageRect( "images/background.jpg", display.actualContentWidth, display.actualContentHeight )
	background.anchorX = 0
	background.anchorY = 0
	background.x = 0 + display.screenOriginX 
	background.y = 0 + display.screenOriginY


	group:insert(background) 


	local titleText = display.newText(group, "You Lost!", _CX, 
		100, "introrustg-base2line.otf", 70)
    titleText:setFillColor( 0, 0, 0 )
    group:insert(titleText)

	local lost = display.newImageRect("images/cat_sprite/cat_lost.png",display.actualContentHeight,display.actualContentHeight)
	lost.anchorX = 0
	lost.anchorY = 0
	lost.x = _CX 
	lost.y = _CY - 200
	lost:scale(0.7,0.7)

	group:insert(lost)

	-- Return button leads to menu scene
	local rtnBtn = widget.newButton{
		label="Main menu",
		labelColor = { default={255}, over={128} },
		fontSize = 50,
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		font = "introheadr-base.otf",
		width = 300, height = 80,
		x = lost.x -280  , y = lost.y + 330,
		onRelease = function ()
		composer:removeScene("scenes.lose")
		composer.gotoScene("scenes.menu","fade",500)
		end
	}

	group:insert(rtnBtn)

	-- Reply button leads to replay scene
	local rplBtn = widget.newButton{
		label="Replay",
		labelColor = { default={255}, over={128} },
		fontSize = 50,
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		font = "introheadr-base.otf",
		width = 300, height = 80,
		x = lost.x -280 , y = lost.y + 150,
		onRelease = function ()
		composer:removeScene("scenes.lose")
		composer.gotoScene("scenes.replay","fade",500)
		end
	}

	group:insert(rplBtn)



end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
	elseif phase == "did" then

	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then

	elseif phase == "did" then
	end	
end


function scene:destroy( event )
	local sceneGroup = self.view
	sceneGroup:removeSelf()
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

composer.removeScene("scene.lose")

-----------------------------------------------------------------------------------------

return scene

