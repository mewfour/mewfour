local composer = require( "composer" )
local widget = require "widget"
local s = require("socket")
local relayout = require('libs.relayout')
local encoder = require('libs.easyEncoder')

local sounds = require('libs.sounds')


local scene = composer.newScene()
local saveProgress = false

function scene:create(event)
	local group = self.view
	local _W, _CX, _CY = relayout._W, relayout._CX, relayout._CY

	local background = display.newImageRect( "images/background.jpg", display.actualContentWidth, display.actualContentHeight )
	background.anchorX = 0
	background.anchorY = 0
	background.x = 0 + display.screenOriginX 
	background.y = 0 + display.screenOriginY


	group:insert(background) 

	local titleText = display.newText(group, "You Win!", _CX, 110, "introrustg-base2line.otf", 70)
    titleText:setFillColor( 0, 0, 0 )
    group:insert(titleText)

	local win = display.newImageRect("images/cat_sprite/cat_win.png",display.actualContentHeight,display.actualContentHeight)
	win.anchorX = 0
	win.anchorY = 0
	win:scale(0.7,0.7)
	win.x = _CX + 30
	win.y = _CY - 120
	

	group:insert(win)



	-- Add save process button (only for winner)
	local saveBtn = widget.newButton{
		label="Save",
		labelColor = { default={255}, over={128} },
		fontSize = 50,
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		font = "introheadr-base.otf",
		width = 300, height = 80,
		x = win.x - 250 , y = win.y + 50,
		onRelease = function ()
			if (saveProgress == false) then
				saveProgress = true
				playerProfile.playerWins = playerProfile.playerWins + 1

				playerProfile.type = "saveProfile"
		    	encoded = encoder.encode(playerProfile)
		    	udp:send(encoded)
		    	print("Profile Saved")
		    end
		-- your code here sir
		-- my pleasure.
		end
	}

	group:insert(saveBtn)	

	-- Add return button 
	local rtnBtn = widget.newButton{
		label="Back",
		labelColor = { default={255}, over={128} },
		fontSize = 50,
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		font = "introheadr-base.otf",
		width = 300, height = 80,
		x = win.x - 250 , y = win.y + 350,
		onRelease = function ()
		composer.removeScene("scenes.win")
		composer.gotoScene("scenes.menu","fade",500)
		end
	}

	group:insert(rtnBtn)

	-- Add replay button
	local rplBtn = widget.newButton{
		label="Replay",
		labelColor = { default={255}, over={128} },
		fontSize = 50,
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		font = "introheadr-base.otf",
		width = 300, height = 80,
		x = win.x - 250 , y = win.y + 200,
		onRelease = function ()
		composer.removeScene("scenes.win")

		composer.gotoScene("scenes.replay","fade",500)
		end
	}
	group:insert(rplBtn)



end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
	elseif phase == "did" then
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
	elseif phase == "did" then
	end	
end


function scene:destroy( event )
	local sceneGroup = self.view
	sceneGroup:removeSelf()

end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )



-----------------------------------------------------------------------------------------

return scene

