-- Main menu

local composer = require( "composer" )
local widget = require "widget"
local s = require("socket")
local relayout = require('libs.relayout')
local sounds = require('libs.sounds')
local encoder = require('libs.easyEncoder')
local scene = composer.newScene()
font = "introheadr-base.otf"
font_h = "introrustg-base2line.otf"

--------------------------------------------
local _W, _CX, _CY = relayout._W, relayout._CX, relayout._CY

-- sound and music buttons
local soundsButtons = {}
local musicButtons = {}

-- player's position (either 1 or 2)
local playerPos = "1"

-- Connection to the server
local connection = false

-- Check if client logged in to server
local loggedIn = false

-- Check if login attempt failed
local loginFail = false

-- Check if profile creation succeeded
local createSuccess = false

-- Check if profile creation failed
local createFail = false

-- Current user id 
local userid = ""

-- Current user password
local password = ""

-- Player's profile
playerProfile = {}

-- Display player's profile
local profileDisplay = false

-- Display player's id
local playerNameText

-- Display player's wins
local scoreText

-- Backview that includes playerNameText and scoreText
local backView = display.newGroup()

-- Initialising playerNameText
local playerNameText = 
	display.newText( backView, "", _CX, _CY - 130, native.systemFontBold, 40)
playerNameText:setFillColor( 0, 0, 0 )

-- Initialising scoreText    
local scoreText = 
	display.newText( backView, "", _CX, _CY - 80, native.systemFontBold, 40)
scoreText:setFillColor( 0, 0, 0 )

-- Change to game scene
local function changeScene()
	local options =
	{
    effect = "fade",
    time = 400,
    params = playerPos
	}

	composer.removeScene("scenes.menu",true)
	composer.gotoScene( "scenes.game", options )
	print("changingP")
end 


-- Change to waiting scene
local function changeScene_wait()
	local options =
	{
    effect = "fade",
    time = 400,
    params = playerPos
	}

	composer.removeScene("scenes.menu",true)
	composer.gotoScene( "scenes.waiting", options )
	print("changingP")
end


-- Receive encoded message from server to retrieve player profile
local function receiveEncodedMsg()
	local data = udp:receive()
	if data then
		decoded = encoder.decode(data)
		playerProfile = decoded
		loggedIn = true
		print("Login successful")
		print("Hello " .. playerProfile.playerId)
	else
		print("Fatal error!")
	end
end


-- Receive message from server and interpret accordingly
local function receiveUdpMsg()
    local data = udp:receive()
    if data then
    	print (data)
   		-- Player position data, assign position 1 to current user
        if (data == "1") then
        	playerPos = "1"
        	print("changing")
       		changeScene_wait()
       		return true

       	-- Player position data assign position 2 to current user
       	elseif (data == "2") then
       		playerPos = "2"
       		print("changing")
       		changeScene()
       		return true

       	-- User has successfully connected to the server
       	elseif (data == "connected") then
       		connection = true
       		print("Connected!")
       		return true

       	-- User has successfully created designated profile
       	elseif (data == "createSuccess") then
       		print("Profile Created!")
       		createSuccess = true
       		return true

       	-- User has failed to create designated profile
       	elseif (data == "createFail") then
       		createFail = true
       		print("Profile Already Exists!")
       		return true

       	-- User has failed to log in
       	elseif (data == "loginFail") then
       		loginFail = true
       	    print("Login failed. ID or Password incorrect")
       	    return true

       	-- User has successfully logged in
        elseif (data == "loginSuccess") then
        	loginFail = false
            receiveEncodedMsg()
            return true
        end
    end

    -- Recursively receive messages (Like an enterFrame listener)
    timer.performWithDelay(100, receiveUdpMsg)
end


-- 'onRelease' event listener for playBtn
local function onPlayBtnRelease()
	if (connection == true and loggedIn == true) then
	    udp:send("start")
	    receiveUdpMsg()
	elseif (connection == true) then
		print("Not logged in")
	else
		print("Not connected!")
	end
	return true	-- indicates successful touch
end


-- 'onRelease' event listener for connectBtn
local function onConnectBtnRelease()	
	udp:send("connect")
	timer.performWithDelay(0, receiveUdpMsg)
	return true	-- indicates successful touch
end

-- Update sounds button
local function updateAndDisplaySndBtn()
	soundsButtons.on.isVisible = false
	soundsButtons.off.isVisible = false
	if sounds.isSoundOn then
		soundsButtons.on.isVisible = true
	else
		soundsButtons.off.isVisible = true
	end
end

-- Display profile creation block
local function displayProfileCreator()
	local profileCreated = false

	-- If not connected to server then do not allow block to be shown
	if (connection == false) then
		print ("Not connected!")
		return false
	end

	-- Profile creator display group
	local profilePage = display.newGroup()

	-- Ooverlay block of profile creator
    local block = 
    	display.newRect(profilePage, _CX, _CY, _CX * 7/4, _CY * 7/4)
	block.fill = {0.7, 1, 1}
	relayout.add(block)
	
	-- Creation status text
	local createText = 
		display.newText(profilePage, "", _CX * 3/2 + 20, _CY*2/3, font, 30)
    createText:setFillColor(0, 0, 0)

    -- "Create Profile" text
	local profileText = 
		display.newText(profilePage, "Create Profile", _CX, 100, font_h, 70)
    profileText:setFillColor( 0, 0, 0 )

    -- "Player ID" text
    local idText = 
    	display.newText(profilePage, "Player ID :", _CX*1/2, _CY *2/3, font, 50)
    idText:setFillColor(0, 0, 0)

    -- "Password" text
    local pwText = 
    	display.newText(profilePage, "Password : ", _CX*1/2, _CY, font, 50)
    pwText:setFillColor(0, 0, 0)

    -- Player id textbox listener
    local function idListener( event )
        if ( event.phase == "editing") then

            userid = event.target.text
            print( userid )
        end
	end

	-- Password textbox listener
	local function pwListener( event )
        if ( event.phase == "editing" ) then
            password = event.target.text
            print( password )
        end
	end
	    
	-- Player id textbox
    idField = native.newTextField(_CX, _CY *2/3 + 2, 300, 55 )
    idField:addEventListener( "userInput", idListener )
    profilePage:insert(idField)

    -- Password textbox
    pwField = native.newTextField( _CX, _CY + 2, 300, 55 )
    pwField:addEventListener( "userInput", pwListener )
    profilePage:insert(pwField)

    -- Check if profile creation is successful (Based on existence of profile)
  	local function checkCreation()
  		if (createSuccess == true) then
  			createText.text = "Profile Created!"
    		profileCreated = true
    		createSuccess = false
  			return true
  		end
  		if (createFail == true) then
    		createText.text = "Profile Already Exists!"
    		createFail = false
  			return false
  		end
  		timer.performWithDelay(100, checkCreation)
  	end

  	-- Create profile button
    createBtn = widget.newButton {
		label="Create",
		labelColor = { default={255}, over={128} },
		fontSize = 50,
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=308, height=80,
		onRelease = function()

			-- Empty ID or Password field
			if (userid == "" or password == "") then
				createText.text = "ID or Password\nnot entered"
    			return false
			end
		    local userData = {}
		    userData.type = "createProfile"
		    userData.playerId = userid
		    userData.password = password
		    userData.playerWins = 0
		    encoded = encoder.encode(userData)
		    udp:send(encoded)
		    timer.performWithDelay(100, receiveUdpMsg)
		    timer.performWithDelay(100, checkCreation)
		end	-- event listener function
	}
	createBtn.x = display.contentCenterX * 3/2 
	createBtn.y = display.contentHeight/2 + 175
    profilePage:insert(createBtn)

    local function closeBtnRelease()
    	createSuccess = false
    	createFail = false	
	    profilePage:removeSelf()
	    return true	-- indicates successful touch
    end

    -- Close profile creator button
	closeBtn = widget.newButton {
		label="Close",
		labelColor = { default={255}, over={128} },
		fontSize = 50,
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=308, height=80,
		onRelease = closeBtnRelease	-- event listener function
	}
	closeBtn.x = display.contentCenterX * 1/2 
	closeBtn.y = display.contentHeight/2 + 175
	profilePage:insert(closeBtn)
end


-- display player profile on menu screen after login
local function displayPlayerProfile()
	playerNameText.text =  "Hello, " .. playerProfile.playerId .. " !"
    
	scoreText.text = "Wins : " .. playerProfile.playerWins
end


-- display login block 
local function displayLogin()
	if (connection == false) then
		print ("Not connected!")
		return false
	end

	if (loggedIn == true) then
		print ("Already connected")
		return false
	end

	local profilePage = display.newGroup()

	-- overlay block
    local block =
    	display.newRect(profilePage, _CX, _CY, _CX * 7/4, _CY * 7/4)
	block.fill = {0.7, 1, 1}
	relayout.add(block)

	-- "Login" text
	local profileText = 
		display.newText(profilePage, "Login", _CX, 100, font_h, 70)
    profileText:setFillColor( 0, 0, 0 )

    -- "Player ID" text
    local idText = 
    	display.newText(profilePage, "Player ID :", _CX*1/2, _CY *2/3, font, 50)
    idText:setFillColor(0, 0, 0)

    -- "Password" text
    local pwText = 
    	display.newText(profilePage, "Password : ", _CX*1/2, _CY, font, 50)
    pwText:setFillColor(0, 0, 0)

    -- login id textbox listener
    local function idListener( event )
        if ( event.phase == "editing") then

            userid = event.target.text
            print( userid )
        end
	end

	-- password textbox listener
	local function pwListener( event )
        if ( event.phase == "editing") then
            password = event.target.text
            print( password )
        end
	end

	-- login id textbox
    idField = native.newTextField(_CX, _CY *2/3 + 2, 300, 55 )
    idField:addEventListener( "userInput", idListener )
    profilePage:insert(idField)

    -- password textbox
    pwField = native.newTextField( _CX, _CY + 2, 300, 55 )
    pwField:addEventListener( "userInput", pwListener )
    profilePage:insert(pwField)

    local function closeBtnRelease()
    	if (profileDisplay == false and loggedIn == true) then
    	    displayPlayerProfile()
    	    profileDisplay = true
    	end	
	    profilePage:removeSelf()
	    return true	-- indicates successful touch
    end

    -- close login block
    local function closePage() 
    	if (loggedIn == true) then
    		closeBtnRelease()
    		return true
    	end
    	if (loginFail == true) then
    		local loginText = 
    			display.newText(profilePage, "ID or Password \nIncorrect"
    							, _CX * 3/2, _CY*2/3, font, 30)
    		loginText:setFillColor(0, 0, 0)
    		return false
    	end
    	return timer.performWithDelay(100, closePage)
    end

    -- Login button
    loginBtn = widget.newButton {
		label="Login",
		labelColor = { default={255}, over={128} },
		fontSize = 50,
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=308, height=80,
		onRelease = function()
		    local userData = {}
		    userData.type = "login"
		    userData.playerId = userid
		    userData.password = password
		    encoded = encoder.encode(userData)
		    udp:send(encoded)
		    timer.performWithDelay(100, receiveUdpMsg)
		    timer.performWithDelay(100, closePage)
		   
		end	-- event listener function
	}
	loginBtn.x = display.contentCenterX * 3/2 
	loginBtn.y = display.contentHeight/2 + 175
    profilePage:insert(loginBtn)

    -- Close login block button
	closeBtn = widget.newButton {
		label="Close",
		labelColor = { default={255}, over={128} },
		fontSize = 50,
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=308, height=80,
		onRelease = closeBtnRelease	-- event listener function
	}
	closeBtn.x = display.contentCenterX * 1/2 
	closeBtn.y = display.contentHeight/2 + 175
	profilePage:insert(closeBtn)
end

-- initialise the instructions page
local function displayInstruction()
	local instructionPage = display.newGroup()
    local block = display.newRect(instructionPage, _CX, _CY, _CX * 7/4, _CY * 7/4)
	block.fill = {0.7, 1, 1}
	relayout.add(block)
	instructionPage:insert(block)

	local profileText = display.newText(instructionPage, "Instructions", _CX, 100, font_h, 70)
    profileText:setFillColor( 0, 0, 0 )

    -- Left key
    local leftText = display.newText(instructionPage, "Left", _CX-170, _CY *2/3, font, 50)
    leftText:setFillColor(0, 0, 0)
    --profilePage.insert(idText)
    local leftImg = display.newImageRect("images/buttons/left arrow.png",display.actualContentHeight,display.actualContentHeight)
	leftImg.anchorX = 0
	leftImg.anchorY = 0
	leftImg:scale(0.1,0.1)
	leftImg.x = _CX-350
	leftImg.y = _CY*2/3-30
	instructionPage:insert(leftImg)

	-- right key
    local rightText = display.newText(instructionPage, "Right", _CX-170, _CY*19/20, font, 50)
    rightText:setFillColor(0, 0, 0)
    local rightImg = display.newImageRect("images/buttons/right arrow.png",display.actualContentHeight,display.actualContentHeight)
	rightImg.anchorX = 0
	rightImg.anchorY = 0
	rightImg:scale(0.1,0.1)
	rightImg.x = _CX-350
	rightImg.y = _CY*19/20-30
	instructionPage:insert(rightImg)

	-- jump key
    local jumpText = display.newText(instructionPage, "Jump", _CX-170, _CY*37/30, font, 50)
    jumpText:setFillColor(0, 0, 0)
    local jumpImg = display.newImageRect("images/buttons/up arrow.png",display.actualContentHeight,display.actualContentHeight)
    jumpImg.anchorX = 0
	jumpImg.anchorY = 0
	jumpImg:scale(0.1,0.1)
	jumpImg.x = _CX-350
	jumpImg.y = _CY*37/30-30
	instructionPage:insert(jumpImg)

	-- weapon inventory
    local weaponText = display.newText(instructionPage, "Weapon inventory", _CX+230, _CY*2/3, font, 50)
    weaponText:setFillColor(0, 0, 0)
    local weaponImg = display.newImageRect("images/buttons/circle.png",display.actualContentHeight,display.actualContentHeight)
    weaponImg.anchorX = 0
	weaponImg.anchorY = 0
	weaponImg:scale(0.1,0.1)
	weaponImg.x = _CX
	weaponImg.y = _CY*2/3-30
	instructionPage:insert(weaponImg)

	-- attack key
    local atkText = display.newText(instructionPage, "Attack", _CX+180, _CY*19/20, font, 50)
    atkText:setFillColor(0, 0, 0)
    local atkImg = display.newImageRect("images/buttons/atk.png",display.actualContentHeight,display.actualContentHeight)
    atkImg.anchorX = 0
	atkImg.anchorY = 0
	atkImg:scale(0.2,0.2)
	atkImg.x = _CX-20
	atkImg.y = _CY*19/20-50
	instructionPage:insert(atkImg)

    local function closeBtnRelease()
	    instructionPage:removeSelf()
    end

   	closeBtn = widget.newButton {
		label="Close",
		labelColor = { default={255}, over={128} },
		fontSize = 50,
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=308, height=80,
		onRelease = closeBtnRelease	-- event listener function
	}
	closeBtn.x = display.contentCenterX
	closeBtn.y = display.contentCenterY+190
	instructionPage:insert(closeBtn)
end

-- Initialise the credits page
local function displayCredits()
	local creditsPage = display.newGroup()
    local block = display.newRect(creditsPage, _CX, _CY, _CX * 7/4, _CY * 7/4)
	block.fill = {0.7, 1, 1}
	relayout.add(block)
	creditsPage:insert(block)

	local creditsText = display.newText(creditsPage, "Credits", _CX, 100, font_h, 70)
    creditsText:setFillColor( 0, 0, 0 )
    local creditsInfoText = display.newText(creditsPage, 
    	"Made by team Mewfour. ",
    	 _CX+50, _CY+105, _CX * 7/4-30, _CY * 7/4-30, font, 50)
    creditsInfoText:setFillColor( 0, 0, 0 )

    local function closeBtnRelease()
	    creditsPage:removeSelf()
	    return true	-- indicates successful touch
    end

   	closeBtn = widget.newButton {
		label="Close",
		labelColor = { default={255}, over={128} },
		fontSize = 50,
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=308, height=80,
		onRelease = closeBtnRelease	-- event listener function
	}
	closeBtn.x = display.contentCenterX
	closeBtn.y = display.contentCenterY+190
	creditsPage:insert(closeBtn)
end

function scene:create( event )
	local sceneGroup = self.view
	local _W, _CX, _CY = relayout._W, relayout._CX, relayout._CY

	-- display a background image
	local background 
		= display.newImageRect( sceneGroup, "images/background.jpg", 
			display.actualContentWidth, display.actualContentHeight )
	background.anchorX = 0
	background.anchorY = 0
	background.x = 0 + display.screenOriginX 
	background.y = 0 + display.screenOriginY
	
	-- Display game title 
	local titleText = display.newText(sceneGroup, "Cat Fighters", _CX, 
		100, font_h, 70)
    titleText:setFillColor( 1,1,1 )

    -- Display status message
    local statusText = display.newText(sceneGroup, "Please connect to server", _CX, _CY, native.systemFontBold, 30)
    statusText:setFillColor( 1,1,1  )
	
	-- Play game button
	playBtn = widget.newButton{
		label="Play Now",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		fontSize= 45,
		width=308, height=80,
		onRelease = 
			function ()
				print("pressed play")
				local function removeMsg()
					statusText.text = ""
				end
				if (connection == false) then
					statusText.text = "Not connected to server"
				else
					if (loggedIn == false) then
						statusText.text = "Please login"
						timer.performWithDelay(5000, removeMsg)
					else
						onPlayBtnRelease()
					end
				end
				return true
			end
	}
	playBtn.x = display.contentCenterX
	playBtn.y = display.contentHeight - 125

	-- Connect to server button
	connectBtn = widget.newButton{
		label="Connect",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		fontSize = 30,
		width=200, height=50,
		onRelease = 
			function()
				local function remove_message()
					statusText.text = ""
				end
				if (connection == true) then
					statusText.text = "Already connected!"
					timer.performWithDelay(2000, remove_message)
					return true
				end
				onConnectBtnRelease()

				local function server_message()
					if (connection == false) then
						statusText.text = "Failed to locate server"
						return false
					else
						statusText.text = "Connected to server!"
						timer.performWithDelay(5000, remove_message)
						return true
					end
				end

				timer.performWithDelay(500, server_message)
				return true
			end
	}
	connectBtn.x = display.contentCenterX * 3/2 + 50
	connectBtn.y = display.contentHeight/2 - 125

	-- Create profile button
	newIDBtn = widget.newButton{
		label="Create ID",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		fontSize = 30,
		width=200, height=50,
		onRelease = 
			function()
				if (connection == true) then
					displayProfileCreator()
					return true
				else
					statusText.text = "Not connected to server"
					return false 
				end
			end
	}
	newIDBtn.x = display.contentCenterX * 3/2 + 50
	newIDBtn.y = display.contentHeight/2 - 65

	-- Login button
	loginBtn = widget.newButton{
		label="Login",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		fontSize = 30,
		width=200, height=50,
		onRelease = 
			function()
				if(connection == false) then
					statusText.text = "Not connected to server"
					return false
				end
				if (loggedIn == true) then
					statusText.text = "Already logged in"
					return false
				end
				displayLogin()
				return true
			end	-- event listener function
	}
	loginBtn.x = display.contentCenterX * 3/2 + 50
	loginBtn.y = display.contentHeight/2 - 5

	-- Instructions button
	instructionsBtn = widget.newButton {
		label="Instructions",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		fontSize = 30,
		width=200, height=50,
		onRelease = 
			function()
				displayInstruction()
			end
	}
	instructionsBtn.x = display.contentCenterX * 3/2 + 50 
	instructionsBtn.y = display.contentHeight/2 + 115

	-- Credits button
	creditsBtn = widget.newButton {
		label="Credits",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		fontSize = 30,
		width=200, height=50,
		onRelease = 
			function()
				displayCredits()
			end
	}
	creditsBtn.x = display.contentCenterX * 3/2 + 50  
	creditsBtn.y = display.contentHeight/2 + 175

	-- Sounds button on
	soundsButtons.on = widget.newButton({
		defaultFile = 'images/buttons/sounds_on.png',
		overFile = 'images/buttons/sounds_on-over.png',
		width = 48, height = 48,
		x = display.screenOriginX + 30, y = display.contentHeight - 30,
		onRelease = function()
			sounds.play('tap')
			sounds.isSoundOn = false
			updateAndDisplaySndBtn()
		end
	})
	soundsButtons.on.isRound = true

	-- Sounds button off
	soundsButtons.off = widget.newButton({
		defaultFile = 'images/buttons/sounds_off.png',
		overFile = 'images/buttons/sounds_off-over.png',
		width = 48, height = 48,
		x = display.screenOriginX + 30, y = display.contentHeight - 30,
		onRelease = function()
			sounds.play('tap')
			sounds.isSoundOn = true
			updateAndDisplaySndBtn()
		end
	})
	soundsButtons.off.isRound = true
	
	-- all display objects must be inserted into group
	sceneGroup:insert( creditsBtn)
	sceneGroup:insert( instructionsBtn)
	sceneGroup:insert( loginBtn)
	sceneGroup:insert( newIDBtn)
	sceneGroup:insert( connectBtn)
	sceneGroup:insert( playBtn )
	sceneGroup:insert( soundsButtons.on )
	sceneGroup:insert( soundsButtons.off )
end

function scene:show( event )
	if (loggedIn == true) then
    	displayPlayerProfile()
    end	
	udp = s.udp()
	udp:settimeout(0)
	address, port = "192.168.1.102", 22345 -- IP address of the server
	udp:setpeername(address, port)
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		playerNameText.text=""
		scoreText.text = ""
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end


function scene:destroy( event )
	--local sceneGroup = self.view
	--sceneGroup:removeSelf()
	
	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene