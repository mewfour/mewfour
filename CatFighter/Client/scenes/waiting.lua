local composer = require( "composer" )
local s = require("socket")
local relayout = require('libs.relayout')
local scene = composer.newScene()
local playerPos = "1"
local playerReady = false
local periodic_send_counter = 50

-- Change to game scene
local function changeScene()
	local options =
	{
    effect = "fade",
    time = 400,
    params = playerPos
	}

	if (playerReady == true) then
		composer.removeScene("scenes.waiting")
		composer.gotoScene( "scenes.game", options )
	end	
end 

-- Receive initial message from server to determine player position
local function receiveUdpMsg(event)
    local data = udp:receive()
    if data then
    	--print ("get data")
    	--print(data)
        if (data == "3") then
			print("receive 3")
        	playerPos = "1"
       		playerReady = true
       		changeScene()
       		return true
        end
    end
    --timer.performWithDelay(100, receiveUdpMsg)
end

function periodic_send(event)
	--local player_coordinates = {x = player.x, y = player.y}

	periodic_send_counter = periodic_send_counter - 1
	if (periodic_send_counter<=0) then
			print("updating server")
		udp:send("hello")
		periodic_send_counter = 50
	end
end


function scene:create( event )
	periodic_send(event)
	local sceneGroup = self.view
	local _W, _CX, _CY = relayout._W, relayout._CX, relayout._CY

	-- display a background image
	local background = display.newImageRect( "images/waiting.png", display.actualContentWidth, display.actualContentHeight )
	background.anchorX = 0
	background.anchorY = 0
	background.x = 0 + display.screenOriginX 
	background.y = 0 + display.screenOriginY

	local waiting = display.newImageRect("images/cat_sprite/cat_waiting2.png",display.actualContentHeight,display.actualContentHeight)
	waiting.anchorX = 0
	waiting.anchorY = 0
	waiting:scale(0.5,0.5)
	waiting.x = _CX-50
	waiting.Y = _CY+200

	sceneGroup:insert( background )
	sceneGroup:insert(waiting)
	Runtime:addEventListener( "enterFrame", receiveUdpMsg)
	Runtime:addEventListener( "enterFrame", periodic_send)

end


function scene:destroy( event )
	print("=============destroy")
	local sceneGroup = self.view
	sceneGroup:removeSelf()
	Runtime:removeEventListener("enterFrame", changeScene)
	Runtime:removeEventListener( "enterFrame", periodic_send)
	Runtime:removeEventListener( "enterFrame", receiveUdpMsg)

end


scene:addEventListener( "create", scene )
scene:addEventListener("destroy", scene)


return scene



