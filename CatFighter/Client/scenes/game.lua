-- Main gameplay scene

-- MAIN SCENE BEFORE GAMEPLAY, THE FIRST STATIONARY SCENE SEEN BY PLAYER
local composer = require('composer')
local physics = require('physics') -- Box2D physics
local widget = require('widget')
local relayout = require('libs.relayout')
local scene = composer.newScene()
local json = require('json')
local decoder = require('libs.easyEncoder')
local s = require("socket")

local newController = require('classes.controller').newController
local newPlayer = require('classes.player').newPlayer
local newOpponent = require('classes.opponent').newOpponent
local newPotion = require('classes.potion').newPotion
local newBoxing = require('classes.boxing').newBoxing
local newTextController = require('libs.textController').newTextController
local newItemTest = require('libs.itemTest').newItemTest
local newReplayTest = require('libs.replayTest').newTest
local replayTest = newReplayTest()

physics.start()
physics.setGravity(0, 150) -- Default gravity is too boring


local gamePause = false

-- Enabling multitouch
system.activate( "multitouch" )


-- Recording path
local self_path =  system.pathForFile("self_data.txt", system.DocumentsDirectory)
local opponent_path = system.pathForFile("opponent_data.txt", system.DocumentsDirectory)

-- Initial variable for recording
local frame_no = 0
local recording = true
local file

local debug = false


-- Record opponent data 
local function recordData(data)
	if (recording) then
		file = io.open(opponent_path, "a")
		file:write("frame:" .. frame_no .. "\n" .. data)
		file:close()
	end
end

-- Get the inisial position
local function setInitialPos(playerPos, oppoPos)
	if (recording) then
		self_file = io.open(self_path, "w")
		--self_file:write("frame:" .. frame_no .."\nx: " .. self.player1.x .. "\ny: " .. self.player1.y .. "\n")
		self_file:write("frame:" .. frame_no .. "\nplayerPos:" .. playerPos .. "\n")

		replayTest:StartReplay(self_file)

		self_file:close()

		opponent_file = io.open(opponent_path, "w")
		--opponent_file:write("frame:" .. frame_no .."\nx: " .. self.opponent.x .. "\ny: " .. self.opponent.y .. "\n")
		opponent_file:write("frame:" .. frame_no .."\nplayerPos:" .. oppoPos .. "\n")

		replayTest:StartReplay(opponent_file)
		opponent_file:close()

	end
end


function scene:create(event)
	-- BACKGROUND SETTINGS
	local _W, _H, _CX, _CY = relayout._W, relayout._H, relayout._CX, relayout._CY
	local playerPos = event.params

	local potion = nil
	local potiontimer = 0
	local boxing = nil
	local boxingtimer = 0

	local group = self.view

	group.gamePause = false


	-- -- BACKGROUND SETTINGS , DISPLAYING COLOUR AND RECTANGLE
	-- display a background image
	local background = display.newImageRect( group, "images/game_background3.jpg", display.actualContentWidth, display.actualContentHeight )
	background.anchorX = 0
	background.anchorY = 0
	background.x = 0 + display.screenOriginX 
	background.y = 0 + display.screenOriginY
	relayout.add(background)

	group:insert(background)

	-- Creating bottom platform
	self.tiles = {}
	local numTiles = math.ceil(_W / 64 / 2)

	-- tileShape is the thin rectangular section which defines the top area of 
	-- the platform. 
	local tileShape = { -24,-30, 24,-30, 24,-32, -24,-32}
	local tileParams = {shape = tileShape, density = 2, friction = 0, bounce = 0}
	for i = -numTiles - 4, numTiles + 4 do -- Add extra 4 on the sides for resize events
		local tile = display.newImageRect(group, 'images/tile.jpg', 64, 64)
		tile.anchorY = 1
		tile.x, tile.y = i * 64 + _CX, _H*3/4
		tile.objType = "ground"
		tile.alpha = 0
		physics.addBody(tile, 'static', tileParams)
		group:insert(tile)
		table.insert(self.tiles, tile)
	end



	-- top left platform
	-- tileblock is the overlapping platform that considers the whole platform as a physical object
	local tileBlockParams = {density = 2, friction = 0, bounce = 0}
	for i = -numTiles, numTiles/2 - 8 do -- Add extra 4 on the sides for resize events
		local tile = display.newImageRect(group, 'images/tile.jpg', 64, 64)
		local tileblock = display.newImageRect(group, 'images/tile.jpg', 64, 64)
		tile.anchorY = 1
		tileblock.anchorY = 1
		tile.x, tile.y = i * 64 + _CX, _H/2
		tileblock.x, tileblock.y = i * 64 + _CX, _H/2
		tile.objType = "ground"
		physics.addBody(tile, 'static', tileParams)
		physics.addBody(tileblock, 'static', tileBlockParams)
		group:insert(tileblock)
		group:insert(tile)
		table.insert(self.tiles, tile)
	end



	-- top right platform
	-- tileblock is the overlapping platform that considers the whole platform as a physical object
	for i = numTiles/2, numTiles do -- Add extra 4 on the sides for resize events
		local tile = display.newImageRect(group, 'images/tile.jpg', 64, 64)
		local tileblock = display.newImageRect(group, 'images/tile.jpg', 64, 64)
		tile.anchorY = 1
		tileblock.anchorY = 1
		tile.x, tile.y = i * 64 + _CX, _H/2
		tileblock.x, tileblock.y = i * 64 + _CX, _H/2
		tile.objType = "ground"
		physics.addBody(tile, 'static', tileParams)
		physics.addBody(tileblock, 'static', tileBlockParams)
		group:insert(tile)
		group:insert(tileblock)
		table.insert(self.tiles, tile)
	end

	self.textController = newTextController(group)

	-- Initial player and opponent position based on setup (Player 1 or Player 2)
	if (playerPos == "1") then	
		oppoPos = "2"
		self.player = newPlayer({g = group, scale = 2.5, x = 64, y = _H/2 + 64,posi = 1, dir = "right",tc = self.textController, posi = playerPos})
		self.player.objType = "player"
		self.opponent = newOpponent({g = group, scale = 2.5, x = _W - 128,posi = 2, y = _H/2 + 64, dir = "left", posi =oppoPos})
		self.opponent.objType = "opponent"
	else
		oppoPos = "1"
		self.player = newPlayer({g = group, scale = 2.5, x = _W - 128, y = _H/2 + 64,posi = 2, dir = "left",tc = self.textController, posi = playerPos})
		self.player.objType = "player"
		self.opponent = newOpponent({g = group, scale = 2.5, x = 64, y = _H/2 + 64,posi = 1, dir = "right", posi = oppoPos})
		self.opponent.objType = "opponent"
	end

	self.controller = newController(self,self.player,playerPos,self.opponent)

	local itemTest = newItemTest(self.player)


	-- Record initial position for player and opponent
	setInitialPos(playerPos, oppoPos)

	group:insert(self.player)
	group:insert(self.opponent)

	local function disableItem(i)
		if (i==1 and potion~=nil) then
			potion.isVisible = false
   			potion.count = -1
			potion:removeSelf()
			potion = nil
			potiontimer = 0
		elseif (i==2 and boxing~=nil) then
			boxing.isVisible = false
   			boxing.count = -1
			boxing:removeSelf()
			boxing = nil
			boxingtimer = 0
		end
	end

	-- receive message from server to update game state
	local function receiveUdpMsg(event)

	-- Record frame number for replay purpose
		frame_no = frame_no + 1

    	local data = udp:receive()
        if (data) then
        	-- Message that indicates the disconnection of opponent
        	if (data == "Opponent disconnected") then
        		if (debug) then print("Opponent disconnected.") end
        		composer.removeScene("scenes.game")
        		composer.gotoScene("scenes.menu")
        	-- Message that declares the victory of the player
        	elseif (data == "win") then
        		if (debug) then print ("win!") end
        		self.player:dropWeapon()
        		composer.removeScene("scenes.game")
        		timer.performWithDelay(500,composer.gotoScene("scenes.win"))
        	-- Message that declares the defeat of the player
        	elseif (data == "lose") then
        		if (debug) then print ("lose!") end
        		self.player:dropWeapon()	
        		composer.removeScene("scenes.game")
        		timer.performWithDelay(500,composer.gotoScene("scenes.lose"))
        	end

        	-- decode json data
        	local decoded = decoder.decode(data)

        	-- If the message is from the opponent
        	if (decoded.direction ~= "server") then
	        	-- jump message
	        	if (decoded.jump == "jump") then
	        		self.opponent:jump()
	        	end

	        	-- idle message
	        	if (decoded.direction == "idle") then
	        		self.opponent.setIdle()
	        	end

	        	-- movement to the right message
	        	if (decoded.direction == "right") then
	        		self.opponent:moveRight()
	        	end

	        	-- movement to the left message
	        	if (decoded.direction == "left") then
	        		self.opponent:moveLeft()
	        	end


	        	if(decoded.direction == "pause") then
	        		self.controller:setPause()
	        		gamePause = true
	        	end

	        	if(decoded.direction == "continue") then
	        		self.controller:continue()
	        		gamePause = false
	        	end


	        	if(decoded.attack == "true") then
	        		--print("op atking")
	        		self.opponent:setAttackTrue()
	        		self.player:setHealth(tonumber(decoded.opHp))
	        	elseif(decoded.attack == "false") then
	        		self.opponent:setAttackFalse()
	        	end

	        	if(decoded.sendMessage == "true") then
	        		self.textController:updateText("opponent",decoded.message)
	        	end

	        	-- update coordinates
	        	if (decoded.send_coords == "true") then
	        		self.opponent.x = decoded.x
	        		self.opponent.y = decoded.y
	        	end

	        	-- the opponent picked up an item
	        	if (decoded.item == "delete") then
	        		-- potion
	       			if (decoded.deleteItem == "1") then
	       				if (self.opponent:getHealth()<=90) then
		       				self.opponent:getDamage(-10)
		       			elseif (self.opponent:getHealth()>90) then
		       				self.opponent.health=100
		       			end
	       				disableItem(1)
	       			-- boxing gloves
	       			elseif (decoded.deleteItem == "2") then
	       				self.opponent:pickUpWeapon()
	       				disableItem(2)
	       			end
	       		end

	       		-- opponent dropped his weapon
	       		if(decoded.deleteItem == "drop") then
	       			self.opponent:dropWeapon()
	       		end

	       	-- the message is from the server
       		elseif (decoded.direction == "server") then
	        	-- message for spawing potion
	       		if (decoded.item == "potion" and gamePause == false) then
	       			-- only two items are allowed to appear on screen at a time
	       			if (potion == nil) then
		       			potion = newPotion({g = group, potionx = decoded.itemPos, potiony=0})
		       			group:insert(potion)
						potiontimer = 1
	       				if (debug) then print("potion", decoded.itemPos) end
	       			end
	       			itemTest:ItemGenerate(potion)
	       		-- message for spawing boxing gloves
	       		elseif (decoded.item == "boxing" and gamePause == false) then
	       			if (boxing == nil) then
		       			boxing = newBoxing({g = group, boxingx = decoded.itemPos, boxingy=0})
		       			group:insert(boxing)
						boxingtimer = 1
	       				if (debug) then print("boxing", decoded.itemPos) end
	       			end
	       			itemTest:ItemGenerate(boxing)
	       		end
       		end
       		-- If recording, save the data to a local file
        	recordData(data)
        end
	end


	-- count the number of frames that the items have been existing in the game. 
	-- If an item has been existing for more than 750 frames, disable the item. 
	local function itemTimer(event)
		if (gamePause == false) then
			if (potiontimer~=0) then
				potion:Continue()
				if (potiontimer>750) then
					disableItem(1)
				else
					potiontimer = potiontimer + 1
				end
			end
			if (boxingtimer~=0) then
				boxing:Continue()
				if (boxingtimer>750) then
					disableItem(2)
				else
					boxingtimer = boxingtimer + 1
				end
			end
		else
			if (potiontimer~=0) then
				potion:Pause()
			end
			if (boxingtimer~=0) then
				boxing:Pause()
			end
		end
	end

	-- Calculate the position of player and items, determine whether a player should pick up the item
	local function itemCollection(event)
		if (potion~=nil and self.player ~= nil) then
			-- If a potion is close enough to the player
			if (math.abs(self.player.x-potion.x)<5 and math.abs(self.player.y-potion.y)<50) then

				local plhp1 = self.player:getHealth()
				if (debug) then print("player picked up potion") end
    			self.player:deleteItemMsg(1)
    			if (tonumber(self.player:getHealth())<=90) then
	    			self.player:setHealth(self.player.getHealth()+10)
	    		elseif (tonumber(self.player:getHealth())>90) then
	    			self.player:setHealth(100)
	    		end
				disableItem(1)
				local plhp2 = self.player:getHealth()

				itemTest:ItemHeal(plhp1,plhp2)
			end
		end
		if (boxing~=nil and self.player ~= nil) then
			-- If a pair of boxing gloves is close enough to the player
			if (math.abs(self.player.x-boxing.x)<5 and math.abs(self.player.y-boxing.y)<50) then
				if (debug) then print("player picked up boxing gloves") end
    			self.player:deleteItemMsg(2)
    			self.player:pickUpWeapon(boxing)
				disableItem(2)
			end
		end
	end

	Runtime:addEventListener( "enterFrame", itemCollection)
	Runtime:addEventListener( "enterFrame", itemTimer)
	Runtime:addEventListener( "enterFrame", receiveUdpMsg)

	-- function to disconnect from server
	function disconnect()
    	udp:close()
	end

end


function scene:Pause()
	gamePause = true
end

function scene:continue()
	gamePause = false
end

function scene:destroy( event )


	Runtime:removeEventListener("enterFrame", itemCollection)
	Runtime:removeEventListener( "enterFrame", itemTimer)
	Runtime:removeEventListener( "enterFrame", receiveUdpMsg)

	if (debug) then print("remove the player") end
	self.player:removeRuntime()
	local group = self.view
	group:removeSelf()


end

scene:addEventListener('create', scene)
scene:addEventListener('destroy', scene)

return scene
