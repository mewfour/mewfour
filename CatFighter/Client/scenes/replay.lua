
-- Replay Scene 
-- replay the saved game

-- MAIN SCENE BEFORE GAMEPLAY, THE FIRST STATIONARY SCENE SEEN BY PLAYER
local composer = require('composer')
local physics = require('physics') -- Box2D physics
local widget = require('widget')
local relayout = require('libs.relayout')
local scene = composer.newScene()



local newOpponent = require('classes.opponent').newOpponent
local newPotion = require('classes.potion').newPotion
local newBoxing = require('classes.boxing').newBoxing
local newTextController = require('libs.textController').newTextController

local newReplayTest = require('libs.replayTest').newTest
local replayTest = newReplayTest()



physics.start()
physics.setGravity(0, 150)

local potion = nil
local potiontimer = 0
local boxing = nil
local boxingtimer = 0

local gamePause = false


local self_path =  system.pathForFile("self_data.txt", system.DocumentsDirectory)
local opponent_path = system.pathForFile("opponent_data.txt", system.DocumentsDirectory)

-- Initial frame numbers
local cur_frame = 1
local max_frame_no
local oppo_frame_no = 2
local self_frame_no = 2
local oppo_frame = {}
local self_frame = {}
local oppo_frame_changed = false
local self_frame_changed = false
local no_change = false

-- Save the text file to a table
function processData(path)
	-- Create a data table for storing all the records
	local data = {}
	-- Save the first frame to the table
	local first = true
	local frame = {}
	local temp = {}

	for line in  io.lines(path) do
		temp = {}
		local i = 1
		for word in string.gmatch(line, "[^:]+") do
			temp[i] = word
		    i = i + 1
		    --print(word)
		end

		if (temp[1] == "frame") then
			if(not first) then
				--print(frame.frame)
				table.insert(data, frame)
			else
				first = false
			end
			frame = {}
			frame[temp[1]] = temp[2]
			--new_frame = true
		else
			frame[temp[1]] = temp[2]
		end
	end
	table.insert(data, frame)
	return data
end

-- Print table data in good fromat
function print_r ( t )  
    local print_r_cache={}
    local function sub_print_r(t,indent)
        if (print_r_cache[tostring(t)]) then
            print(indent.."*"..tostring(t))
        else
            print_r_cache[tostring(t)]=true
            if (type(t)=="table") then
                for pos,val in pairs(t) do
                    if (type(val)=="table") then
                        print(indent.."["..pos.."] => "..tostring(t).." {")
                        sub_print_r(val,indent..string.rep(" ",string.len(pos)+8))
                        print(indent..string.rep(" ",string.len(pos)+6).."}")
                    elseif (type(val)=="string") then
                        print(indent.."["..pos..'] => "'..val..'"')
                    else
                        print(indent.."["..pos.."] => "..tostring(val))
                    end
                end
            else
                print(indent..tostring(t))
            end
        end
    end
    if (type(t)=="table") then
        print(tostring(t).." {")
        sub_print_r(t,"  ")
        print("}")
    else
        sub_print_r(t,"  ")
    end
    print()
end

-- find the max frame number the game reached
function findMaxFrame(oppo_table, self_table)

	local o = tonumber(oppo_table[#oppo_table].frame)
	local s = tonumber(self_table[#self_table].frame)
	if (o>s) then
		max_frame_no = o
	else
		max_frame_no = s
	end
	return max_frame_no
end

-- Create the replay scene
function scene:create(event)
	-- BACKGROUND SETTINGS
	local _W, _H, _CX, _CY = relayout._W, relayout._H, relayout._CX, relayout._CY

	local group = self.view
	group.gamePause = false
	self.textController = newTextController(group)


	-- Initial the frame for oppo and self
	oppo_frame = oppo_table[oppo_frame_no]
	self_frame = self_table[self_frame_no]


	-- BACKGROUND SETTINGS , DISPLAYING COLOUR AND RECTANGLE
	local background = display.newImageRect( group, "images/game_background3.jpg", display.actualContentWidth, display.actualContentHeight )
	background.anchorX = 0
	background.anchorY = 0
	background.x = 0 + display.screenOriginX 
	background.y = 0 + display.screenOriginY
	relayout.add(background)

	group:insert(background)

	-- Creating bottom platform
	self.tiles = {}
	local numTiles = math.ceil(_W / 64 / 2)

	-- tileShape is the thin rectangular section which defines the top area of 
	-- the platform. 
	local tileShape = { -24,-30, 24,-30, 24,-32, -24,-32}
	local tileParams = {shape = tileShape, density = 2, friction = 0, bounce = 0}
	for i = -numTiles - 4, numTiles + 4 do -- Add extra 4 on the sides for resize events
		local tile = display.newImageRect(group, 'images/tile.jpg', 64, 64)
		tile.anchorY = 1
		tile.x, tile.y = i * 64 + _CX, _H*3/4
		tile.objType = "ground"
		tile.alpha = 0
		physics.addBody(tile, 'static', tileParams)
		group:insert(tile)
		table.insert(self.tiles, tile)
	end

	-- top left platform
	-- tileblock is the overlapping platform that considers the whole platform as a physical object
	local tileBlockParams = {density = 2, friction = 0, bounce = 0}
	for i = -numTiles, numTiles/2 - 8 do -- Add extra 4 on the sides for resize events
		local tile = display.newImageRect(group, 'images/tile.jpg', 64, 64)
		local tileblock = display.newImageRect(group, 'images/tile.jpg', 64, 64)
		tile.anchorY = 1
		tileblock.anchorY = 1
		tile.x, tile.y = i * 64 + _CX, _H/2
		tileblock.x, tileblock.y = i * 64 + _CX, _H/2
		tile.objType = "ground"
		physics.addBody(tile, 'static', tileParams)
		physics.addBody(tileblock, 'static', tileBlockParams)
		group:insert(tile)
		table.insert(self.tiles, tile)
	end

	-- top right platform
	-- tileblock is the overlapping platform that considers the whole platform as a physical object
	for i = numTiles/2, numTiles do -- Add extra 4 on the sides for resize events
		local tile = display.newImageRect(group, 'images/tile.jpg', 64, 64)
		local tileblock = display.newImageRect(group, 'images/tile.jpg', 64, 64)
		tile.anchorY = 1
		tileblock.anchorY = 1
		tile.x, tile.y = i * 64 + _CX, _H/2
		tileblock.x, tileblock.y = i * 64 + _CX, _H/2
		tile.objType = "ground"
		physics.addBody(tile, 'static', tileParams)
		physics.addBody(tileblock, 'static', tileBlockParams)
		group:insert(tile)
		table.insert(self.tiles, tile)
	end


	-- Add return button 
	local rtnBtn = widget.newButton{
		defaultFile = "images/buttons/restart.png",
		overFile = "images/buttons/restart-over.png",
		width = 40, height = 40,
		x = _W - 30 , y = 30,
		onRelease = function ()
			composer.removeScene("scenes.replay")
			composer.gotoScene("scenes.menu","fade",500)
		end
	}
	group:insert(rtnBtn)


	-- Create two sprite on the scene according to their initial position
	if (selfPos == "1") then
		self.player = newOpponent({g = group, scale = 2.5, x = 64, y = _H/2, dir = "right",tc = self.textController, posi = selfPos})
		self.player.objType = "player"
		self.opponent = newOpponent({g = group, scale = 2.5, x = _W - 128, y = _H/2, dir = "left", posi = "2"})
		self.opponent.objType = "opponent"
	else
		self.player = newOpponent({g = group, scale = 2.5, x = _W - 128, y = _H/2, dir = "left",tc = self.textController,posi = selfPos})
		self.player.objType = "player"
		self.opponent = newOpponent({g = group, scale = 2.5, x = 64, y = _H/2, dir = "right", posi = "1"})
		self.opponent.objType = "opponent"
	end


	group:insert(self.player)
	group:insert(self.opponent)
	
	maxHealth = self.player:getHealth()
	maxOpHealth = self.opponent:getHealth()

	local healthBar1
	local damageBar1
	local healthBar2
	local damageBar2

	healthBar1 = display.newRect(display.screenOriginX + maxHealth*3,display.screenOriginY + _H/10, maxHealth * 3, _H / 20)
	damageBar1 = display.newRect(display.screenOriginX + maxHealth*(3+(3/2)),display.screenOriginY + _H/10, 0, _H / 20)

	healthBar2 = display.newRect(_W - maxOpHealth*3,display.screenOriginY + _H/10, maxOpHealth * 3, _H / 20)
	damageBar2 = display.newRect(_W - maxOpHealth*(3+(3/2)),display.screenOriginY + _H/10, 0, _H / 20)

	healthBar1:setFillColor( 000/255, 255/255, 0/255 )
	healthBar1.strokeWidth = 1
	healthBar1:setStrokeColor( 255, 255, 255, .5 )

	damageBar1:setFillColor( 255/255, 0/255, 0/255 )


	healthBar2:setFillColor( 000/255, 255/255, 0/255 )
	healthBar2.strokeWidth = 1
	healthBar2:setStrokeColor( 255, 255, 255, .5 )

	damageBar2:setFillColor( 255/255, 0/255, 0/255 )

	group:insert(healthBar1)
	group:insert(damageBar1)
	group:insert(healthBar2)
	group:insert(damageBar2)


	-- From controller class
	function updateDamageBar1()
		local currentHealth = 0
		if(selfPos == "1") then
			currentHealth = self.player:getHealth()

		elseif(selfPos == "2") then
			currentHealth = self.opponent:getHealth()
		end
		damageBar1.x = display.screenOriginX + maxHealth*(3+(3/2)) - (maxHealth - currentHealth)*3/2
		damageBar1.width = (maxHealth - currentHealth) * 3
	end

	function updateDamageBar2()
		local currentHealth = 0
		if(selfPos == "1") then
			currentHealth = self.opponent:getHealth()
		elseif(selfPos == "2") then
			currentHealth = self.player:getHealth()
		end
		damageBar2.x = _W - maxOpHealth*(3+(3/2)) + (maxOpHealth - currentHealth)*3/2
		damageBar2.width = (maxOpHealth - currentHealth) * 3

	end

	local function disableItem(i)
		if (i==1 and potion~=nil) then
			potion.isVisible = false
   			potion.count = -1
			potion:removeSelf()
			potion = nil
			potiontimer = 0
		elseif (i==2 and boxing~=nil) then
			boxing.isVisible = false
   			boxing.count = -1
			boxing:removeSelf()
			boxing = nil
			boxingtimer = 0
		end
	end

	local function itemTimer(event)
		if(gamePause == false) then
			if (potiontimer~=0) then
				potion:Continue()
				if (potiontimer>750) then
					disableItem(1)
				else
					potiontimer = potiontimer + 1
				end
			end
			if (boxingtimer~=0) then
				boxing:Continue()
				if (boxingtimer>750) then
					disableItem(2)
				else
					boxingtimer = boxingtimer + 1
				end
			end
		else
			if (potiontimer~=0) then
				potion:Pause()
			end
			if (boxingtimer~=0) then
				boxing:Pause()
			end
		end
	end

	local function itemCollection(event)
		if (potion~=nil) then
			if (math.abs(self.player.x-potion.x)<5 and math.abs(self.player.y-potion.y)<50) then
				print("player picked up potion")
    			self.player:deleteItemMsg(1)
    			if (tonumber(self.player:getHealth())<=90) then
	    			self.player:setHealth(self.player.getHealth()+10)
	    		elseif (tonumber(self.player:getHealth())>90) then
	    			self.player:setHealth(100)
	    		end
    			--print("send msg")
				disableItem(1)
			end
		end
		if (boxing~=nil) then
			if (math.abs(self.player.x-boxing.x)<5 and math.abs(self.player.y-boxing.y)<50) then
				print("player picked up boxing gloves")
    			self.player:deleteItemMsg(2)
    			self.player:pickUpWeapon(boxing)
    			--print("send msg")
				disableItem(2)
			end
		end
	end

	-- Update the player movement 
	function updatePlayer(event)
		-- If have not reached the last frame of the game
		if (cur_frame <= max_frame_no) then
			-- If has not reach the last frame of the opponent move frame
			if (oppo_frame_no < #oppo_table) then
				-- If the last frame has been performed
				if (oppo_frame_changed) then
					oppo_frame_no = oppo_frame_no + 1
					oppo_frame = oppo_table[oppo_frame_no]
					oppo_frame_changed = false
				end
			end

			-- If has not reach the last frame of the self move frame
			if (self_frame_no < #self_table) then
				if (self_frame_changed) then
					self_frame_no = self_frame_no + 1
					self_frame = self_table[self_frame_no]
					self_frame_changed = false
				end
			end

			-- If there are message received in this frame
			if (tonumber(oppo_frame.frame) == cur_frame) then
				print("oppo find " .. cur_frame)
				-- If the frame is not from server
				if (oppo_frame.direction ~= "server") then
		        	-- jump message
		        	if (oppo_frame.jump == "jump") then
		        		self.opponent:jump()
		        	end

		        	-- idle message
		        	if (oppo_frame.direction == "idle") then
		        		self.opponent.setIdle()
		        	end

		        	-- movement to the right message
		        	if (oppo_frame.direction == "right") then
		        		self.opponent:moveRight()
		        	end

		        	-- movement to the left message
		        	if (oppo_frame.direction == "left") then
		        		self.opponent:moveLeft()
		        	end

		        	if(oppo_frame.direction == "pause") then
		        		gamePause = true
		        	end

		        	if(oppo_frame.direction == "continue") then
		        		gamePause = false
		        	end

		        	if(oppo_frame.attack == "true") then
		        		self.opponent:setAttackTrue()
		        		self.player:setHealth(tonumber(oppo_frame.opHp))

		        	elseif(oppo_frame.attack == "false") then
		        		self.opponent:setAttackFalse()
		        	end

		        	-- update coordinates
		        	if (oppo_frame.send_coords == "true") then
		        		self.opponent.x = oppo_frame.x
		        		self.opponent.y = oppo_frame.y
		        	end

		        	if (oppo_frame.item == "delete") then
		       			if (oppo_frame.deleteItem == "1") then
		       				if (self.opponent:getHealth()<=90) then
			       				self.opponent:getDamage(-10)
			       			elseif (self.opponent:getHealth()>90) then
			       				self.opponent.health=100
			       			end
		       				disableItem(1)
		       			elseif (oppo_frame.deleteItem == "2") then
	       					self.opponent:pickUpWeapon()
		       				disableItem(2)
		       			end
		       		end

		       		if(oppo_frame.deleteItem == "drop") then
	       				self.opponent:dropWeapon()
	       			end


	       		elseif (oppo_frame.direction == "server") then
		        	-- check if the message is for item spawning
	       			if (oppo_frame.item == "potion" and gamePause == false) then
		       		-- if (oppo_frame.item == "spawn") then
		       			-- only two items are allowed to appear on screen at a time
		       			if (potion == nil) then
			       			potion = newPotion({g = group, potionx = oppo_frame.itemPos, potiony=0})
			       			group:insert(potion)
							potiontimer = 1
		       				--print("item1", oppo_frame.itemPos)
		       			end
	       			elseif (oppo_frame.item == "boxing" and gamePause == false) then
		       			if (boxing == nil) then
			       			boxing = newBoxing({g = group, boxingx = oppo_frame.itemPos, boxingy=0})
			       			group:insert(boxing)
							boxingtimer = 1
		       				--print("boxing", oppo_frame.itemPos)
		       			end
		       		end
	       		end
				oppo_frame_changed = true

				if (oppo_frame_no ~= #oppo_table and oppo_table[oppo_frame_no + 1].frame == oppo_frame.frame) then
					no_change = true
				end

			end

			if (tonumber(self_frame.frame) == cur_frame) then
				print("self find " .. cur_frame)

	        	if (self_frame.jump == "jump") then
	        		self.player:jump()
	        	end

	        	-- idle message
	        	if (self_frame.direction == "idle") then
	        		self.player.setIdle()
	        	end

	        	-- movement to the right message
	        	if (self_frame.direction == "right") then
	        		self.player:moveRight()
	        	end

	        	-- movement to the left message
	        	if (self_frame.direction == "left") then
	        		self.player:moveLeft()
	        	end

	        	if(self_frame.attack == "true") then
	        		--print("op atking")
	        		self.player:setAttackTrue()
	        		self.opponent:setHealth(tonumber(self_frame.opHp))
	        		--self.player:getDamage(tonumber(oppo_frame.opHp))
	        	elseif(self_frame.attack == "false") then
	        		self.player:setAttackFalse()
	        	end

	        	-- update coordinates
	        	if (self_frame.send_coords == "true") then
	        		self.player.x = self_frame.x
	        		self.player.y = self_frame.y
	        	end

	        	if (self_frame.item == "delete") then
	       			if (self_frame.deleteItem == "1") then
	       				if (self.player:getHealth()<=90) then
		       				self.player:getDamage(-10)
		       			elseif (self.player:getHealth()>90) then
		       				self.player.health=100
		       			end
	       				disableItem(1)
	       			elseif (self_frame.deleteItem == "2") then
	       				self.player:pickUpWeapon()
	       				disableItem(2)
	       			end
	       		end

	       		if(self_frame.deleteItem == "drop") then
	       			self.player:dropWeapon()
	       		end

				self_frame_changed = true
				if (self_frame_no ~= #self_table and self_table[self_frame_no + 1].frame == self_frame.frame) then
					no_change = true
				end 
			end

		else
			return scene
		end
		updateDamageBar1()
		updateDamageBar2()

		if (not no_change) then
			cur_frame = cur_frame + 1
		else
			no_change = false
		end

		--print(cur_frame)
	end
	Runtime:addEventListener( "enterFrame", updatePlayer)
end

replayTest:StartRead(opponent_path)
oppo_table = processData(opponent_path)
--print_r(oppo_table)

replayTest:StartRead(self_path) 
self_table = processData(self_path)
--print_r(self_table) 

oppoPos = oppo_table[1].playerPos -- initial pos
print("oppoPos" .. tostring(oppoPos))
selfPos = self_table[1].playerPos -- initial pos
print("selfPos" .. tostring(selfPos))


findMaxFrame(oppo_table, self_table)
print("max_frame_no is " .. max_frame_no)

function scene:destroy(event)
	local group = self.view

	group:removeSelf()
end


scene:addEventListener('create')
scene:addEventListener('destroy')


return scene