-----------------------------------------------------------------------------------------
--
-- main.lua
-- Create a list and do pair matching
--
-----------------------------------------------------------------------------------------
local clients = {}
local pair = {}
local numberOfClients = 5

function generateIP()
	return math.random(100)
end


function printList ( t )  
    local print_r_cache={}

    local function sub_printList(t,indent)
        if (print_r_cache[tostring(t)]) then
            print(indent.."*"..tostring(t))
        else
            print_r_cache[tostring(t)]=true
            if (type(t)=="table") then
                for pos,val in pairs(t) do
                    if (type(val)=="table") then
                        print(indent.."["..pos.."] => "..tostring(t).." {")
                        sub_printList(val,indent..string.rep(" ",string.len(pos)+8))
                        print(indent..string.rep(" ",string.len(pos)+6).."}")
                    elseif (type(val)=="string") then
                        print(indent.."["..pos..'] => "'..val..'"')
                    else
                        print(indent.."["..pos.."] => "..tostring(val))
                    end
                end
            else
                print(indent..tostring(t))
            end
        end
    end

    if (type(t)=="table") then
        print(tostring(t).." {")
        sub_printList(t,"  ")
        print("}")
    else
        sub_printList(t,"  ")
    end
    print()
end



local n = 1
while (n <= numberOfClients) do
	local newIP = generateIP()
	print (newIP)
	if ( (n-1) % 2 == 0) then -- if this new IP is the odd arrive, create a new pair and put it as the first
		local pair_cache = {}
		pair_cache[1] = newIP 
		clients[ math.floor((n-1) / 2) + 1 ] = pair_cache -- add the new pair to clients list
	else -- if this new IP is a even arrive, put it as the second element of the pair and add the pair to the clients list
		clients[ math.floor((n-1) / 2) + 1 ][2] = newIP

	end
	n = n + 1
end


-- local clients = {}
-- local pair1 = {1,2}
-- local pair2 = {3,4}

-- clients[1] = pair1
-- clients[2] = pair2

-- local c = {{}}
-- c[1][1] = 1
-- c[1][2] = 2
-- c[2][1] = 3

 printList(clients)
-- print_r(c)



