local socket = require("socket")
local json = require("json")
local loadsave = require('libs.loadsave')
local encoder = require('libs.easyEncoder')
udp=socket.udp()

udp:setsockname("*", 22345)
udp:settimeout(0)


ips = {}   -- ip
ports = {} -- ip - port

waitingIp = nil
waitingPort = nil

-- Send message to specific ip and port
local function sendMsg(data, ip, port) 
	udp:sendto(data, ip, port)
end

-- Find port with respective Ip 
local function findVal(table, key, port)
	for i=1,#table do
		if (table[i][1]==key) then
			if (table[i][2] ~= port) then
				return table[i][2]
			end
		end
	end
	return nil
end

-- Find index of element in table
local function findIndex(table, element)
	for i=1, #table do
		if (table[i]==element) then
			return i
		end
	end
	return nil
end

-- Print all elements in a list
local function printList(list)
	for i=1,#list do
		print(list[i])
	end
end

-- Client matching function
local function matching(ip, port)
	print("Connection from: ", ip, port)

	-- First client found
	if (waitingIp==nil) then
		waitingIp = ip
		waitingPort = port
		sendMsg("1", ip, port)
		print("First client found")

	-- Second client found
	else
		sendMsg("2", ip, port)
		table.insert(ports, #ports+1, {waitingIp, waitingPort})
		table.insert(ports, #ports+1, {ip, port})
		table.insert(ips, #ips+1, waitingIp)
		table.insert(ips, #ips+1, ip)
		waitingIp = nil
		waitingPort = nil
		print("Second client found")
		print("new pair made")
	end
	printList(ips)
end

local function retrieveProfile(playerData)
	data = loadsave.loadTable(playerData.playerId .. ".json", 
		   system.DocumentsDirectory)
	if data then
	    if data.password == playerData.password then
	    	return data
	    end
	end
	return nil	
end

local function createProfile(playerData)
	if (loadsave.loadTable(playerData.playerId .. ".json", 
		system.DocumentsDirectory) == nil) then

	    loadsave.saveTable(playerData, playerData.playerId .. ".json", 
	    	system.DocumentsDirectory)
	    return true
	else
		return false
	end
end
-- Enter frame receive message listener
-- Send message from client 1 to client 2 and vice versa
local function receiveUdpMsg(e)
	data, ip, port = udp:receivefrom()
	if data then
		local decoded = encoder.decode(data)
		--if (decoded.type == "connect") then
			--matching(ip, port)]]
		if (data == "connect") then
		    sendMsg("connected", ip, port)
		elseif (data == "start") then
			matching(ip, port)
		elseif (decoded.type == "createProfile") then
			local profile = createProfile(decoded)
			if (profile == true) then
				sendMsg("createSuccess", ip, port)
			else
				sendMsg("createFail", ip, port)
			end
		elseif (decoded.type == "login") then
			local profile = retrieveProfile(decoded)
			if (profile ~= nil) then
				sendMsg("loginSuccess", ip, port)
				sendMsg(encoder.encode(profile), ip, port)
            else
            	sendMsg("loginFail", ip, port)
            end
		else
			if ((findIndex(ips, ip)%2)~=0) then -- odd
				sendMsg(data, ips[findIndex(ips, ip)+1], 
				    findVal(ports, ips[findIndex(ips, ip)+1], port)) --------
			else --even
				sendMsg(data, ips[findIndex(ips, ip)-1], 
					findVal(ports, ips[findIndex(ips, ip)-1], port))
			end
		end
	end

	timer.performWithDelay(0, receiveUdpMsg)
end

timer.performWithDelay(0, receiveUdpMsg)