-- Easy Encoder
-- This library includes encoding and decoding functions based on 
-- spacing. 
-- The encoder accepts a table (with non boolean values) and converts it 
-- into a string separated by colon and newlinea
-- The decoder performs the inverse
-- Encoding format example:
-- a = {x = "10", y = ":20", z = "a b c"}
-- Encoded string = "x:10\ny::20\nz:a b c\n"

local _M = {}

-- sPairs helper function from http://stackoverflow.com/questions/15706270/sort-a-table-in-lua
function sPairs(t, order)
    -- collect the keys
    local keys = {}
    for k in pairs(t) do keys[#keys+1] = k end

    -- if order function given, sort by it by passing the table and keys a, b,
    -- otherwise just sort the keys 
    if order then
        table.sort(keys, function(a,b) return order(t, a, b) end)
    else
        table.sort(keys)
    end

    -- return the iterator function
    local i = 0
    return function()
        i = i + 1
        if keys[i] then
            return keys[i], t[keys[i]]
        end
    end
end

-- Encode the table into a string 
-- Encoding format example:
-- a = {x = "10", y = ":20", z = "a b c"}
-- Encoded string = "x:10\ny::20\nz:a b c\n"
function _M.encode(t)
	local encoded = ""
	for i, j in sPairs(t, function(t,a,b) return b > a end) do
		encoded = encoded .. i .. ":" .. j .. "\n" 
	end
	return encoded
end

-- Decodes the encoded string inversely
function _M.decode(str)
	local decoded = {}
	local currentString = ""
	local key = ""
	local value = ""
	local addChar = false
	local pairFound = 0

	-- Iterate through the string
	for i = 1, string.len(str) do
		local currentChar = string.sub(str,i ,i)
		local addChar = true

		-- : separator found, set key and do not concatenate ":"
		if (currentChar == ":") then
			key = currentString
			currentString = ""
			addChar = false
			pairFound = pairFound + 1

	    -- newline separator found, set value and do not concatenate "\n"
		elseif (currentChar == "\n") then
			value = currentString
			currentString = ""
			addChar = false
			pairFound = pairFound + 1
		end

		-- no separators found, add to current string
		if (addChar == true) then
			currentString = currentString .. currentChar
		end

		-- new key and value pair found, add into decoded table
		if (pairFound == 2) then
			decoded[key] = value
			pairFound = 0
		end
	end
	return decoded
end

return _M