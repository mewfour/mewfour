-- Main gameplay scene

-- MAIN SCENE BEFORE GAMEPLAY, THE FIRST STATIONARY SCENE SEEN BY PLAYER
local composer = require('composer')
local physics = require('physics') -- Box2D physics
local widget = require('widget')
local relayout = require('libs.relayout')
local scene = composer.newScene()
local json = require('json')
local decoder = require('libs.easyEncoder')
local s = require("socket")

local newController = require('classes.controller').newController
local newPlayer = require('classes.player').newPlayer
local newOpponent = require('classes.opponent').newOpponent
physics.start()
physics.setGravity(0, 150) -- Default gravity is too boring

function scene:create(event)
	-- BACKGROUND SETTINGS
	local _W, _H, _CX, _CY = relayout._W, relayout._H, relayout._CX, relayout._CY
	local playerPos = event.params

	-- receive message from server to update game state
	local function receiveUdpMsg(event)
    local data = udp:receive()
        if (data) then

        	-- decode json data
        	local decoded = decoder.decode(data)

        	-- jump message
        	if (decoded.jump == "jump") then
        		self.opponent:jump()
        	end

        	-- idle message
        	if (decoded.direction == "idle") then
        		self.opponent.setIdle()
        	end

        	-- movement to the right message
        	if (decoded.direction == "right") then
        		self.opponent:moveRight()
        	end

        	-- movement to the left message
        	if (decoded.direction == "left") then
        		self.opponent:moveLeft()
        	end

        	-- update coordinates
        	if (decoded.send_coords == "true") then
        		self.opponent.x = decoded.x
        		self.opponent.y = decoded.y
        	end
        end
	end
	Runtime:addEventListener( "enterFrame", receiveUdpMsg)

	-- function to disconnect from server
	function disconnect()
    	udp:close()
	end

	local group = self.view

	-- BACKGROUND SETTINGS , DISPLAYING COLOUR AND RECTANGLE
	local background = display.newRect(group, _CX, _CY, _W, _H)
	background.fill = {
	    type = 'gradient',
	    color1 = {0.2, 0.45, 0.8},
	    color2 = {0.7, 0.8, 1}
	}
	relayout.add(background)

	-- Creating bottom platform
	self.tiles = {}
	local numTiles = math.ceil(_W / 64 / 2)

	-- tileShape is the thin rectangular section which defines the top area of 
	-- the platform. 
	local tileShape = { -24,-30, 24,-30, 24,-32, -24,-32}
	local tileParams = {shape = tileShape, density = 2, friction = 0, bounce = 0}
	for i = -numTiles - 4, numTiles + 4 do -- Add extra 4 on the sides for resize events
		local tile = display.newImageRect(group, 'images/green_tiles/3.png', 64, 64)
		tile.anchorY = 1
		tile.x, tile.y = i * 64 + _CX, _H
		tile.objType = "ground"
		physics.addBody(tile, 'static', tileParams)
		table.insert(self.tiles, tile)
	end

	-- top left platform
	-- tileblock is the overlapping platform that considers the whole platform as a physical object
	local tileBlockParams = {density = 2, friction = 0, bounce = 0}
	for i = -numTiles, numTiles/2 - 8 do -- Add extra 4 on the sides for resize events
		local tile = display.newImageRect(group, 'images/green_tiles/3.png', 64, 64)
		local tileblock = display.newImageRect(group, 'images/green_tiles/3.png', 64, 64)
		tile.anchorY = 1
		tileblock.anchorY = 1
		tile.x, tile.y = i * 64 + _CX, _CY* 3/2
		tileblock.x, tileblock.y = i * 64 + _CX, _CY* 3/2
		tile.objType = "ground"
		physics.addBody(tile, 'static', tileParams)
		physics.addBody(tileblock, 'static', tileBlockParams)
		table.insert(self.tiles, tile)
	end

	-- top right platform
	-- tileblock is the overlapping platform that considers the whole platform as a physical object
	for i = numTiles/2, numTiles do -- Add extra 4 on the sides for resize events
		local tile = display.newImageRect(group, 'images/green_tiles/3.png', 64, 64)
		local tileblock = display.newImageRect(group, 'images/green_tiles/3.png', 64, 64)
		tile.anchorY = 1
		tileblock.anchorY = 1
		tile.x, tile.y = i * 64 + _CX, _CY* 3/2
		tileblock.x, tileblock.y = i * 64 + _CX, _CY* 3/2
		tile.objType = "ground"
		physics.addBody(tile, 'static', tileParams)
		physics.addBody(tileblock, 'static', tileBlockParams)
		table.insert(self.tiles, tile)
	end

	-- Initial player and opponent position based on setup (Player 1 or Player 2)
	if (playerPos == "1") then
		self.player1 = newPlayer({g = group, scale = 2.5, x = 64, y = _H - 128, dir = "right"})	
		self.opponent = newOpponent({g = group, scale = 2.5, x = _W - 128, y = _H - 128, dir = "left"})
	else
		self.player1 = newPlayer({g = group, scale = 2.5, x = _W - 128, y = _H - 128, dir = "left"})	
		self.opponent = newOpponent({g = group, scale = 2.5, x = 64, y = _H - 128, dir = "right"})
	end

	self.controller = newController({g = group, kr = self.player1.keyRight, kl = self.player1.keyLeft})

	group:insert(self.player1)

end

scene:addEventListener('create')
--scene:addEventListener('show')
--scene:addEventListener('hide')

return scene