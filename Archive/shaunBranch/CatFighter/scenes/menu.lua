-- Main menu

local composer = require( "composer" )
local widget = require "widget"
local s = require("socket")
local relayout = require('libs.relayout')
local sounds = require('libs.sounds')
local encoder = require('libs.easyEncoder')
local scene = composer.newScene()

udp = s.udp()
udp:settimeout(0)

--------------------------------------------
local _W, _CX, _CY = relayout._W, relayout._CX, relayout._CY
-- forward declarations and other locals
local playBtn
local soundsButtons = {}
local musicButtons = {}

-- player position
local playerPos = "1"
local playerReady = false
local connection = false
local loggedIn = false
local userid = ""
local password = ""
local playerProfile = {}
local profileDisplay = false
local playerNameText
local scoreText
local backView = display.newGroup()

-- Change to game scene
local function changeScene()
	local options =
	{
    effect = "fade",
    time = 400,
    params = playerPos
	}

	if (playerReady == true) then
		composer.gotoScene( "scenes.game", options )
	end	
end 

-- Receive initial message from server to determine player position
local function receiveEncodedMsg()
	data = udp:receive()
	if data then
		decoded = encoder.decode(data)
		playerProfile = decoded
		loggedIn = true
		print("Login successful")
		print("Hello " .. playerProfile.playerId)

	else
		print("Fatal error!")
	end
end
    
local function receiveUdpMsg(event)
    data = udp:receive()
    if data then
        if (data == "1") then
        	playerPos = "1"
       		playerReady = true
       		changeScene()
       		return true
       	elseif (data == "2") then
       		playerPos = "2"
       		playerReady = true
       		changeScene()
       		return true
       	elseif (data == "connected") then
       		connection = true
       		print("Connected!")
       		return true
       	elseif (data == "createSuccess") then
       		print("Profile Created!")
       		return true
       	elseif (data == "createFail") then
       		print("Profile Already Exists!")
       		return
       	elseif (data == "loginFail") then
       	    print("Login failed. ID or Password incorrect")
        elseif (data == "loginSuccess") then
            receiveEncodedMsg()
        end
    end
    timer.performWithDelay(100, receiveUdpMsg)
end

-- 'onRelease' event listener for playBtn
local function onPlayBtnRelease()	
	--local address, port = "59.102.43.179", 12345
	--local address, port = "10.9.131.242", 22345
	--local address, port = "10.12.253.195", 12345
	--udp:setpeername(address, port)
	if (connection == true and loggedIn == true) then
	    udp:send("start")
	    timer.performWithDelay(100, receiveUdpMsg)
	elseif (connection == true) then
		print("Not logged in")
	else
		print("Not connected!")
	end
	return true	-- indicates successful touch
end

-- 'onRelease' event listener for playBtn
local function onConnectBtnRelease()	
	--local address, port = "59.102.43.179", 12345
	local address, port = "127.0.0.1", 22345
	--local address, port = "10.12.253.195", 12345
	udp:setpeername(address, port)
	udp:send("connect")
	timer.performWithDelay(0, receiveUdpMsg)
	return true	-- indicates successful touch
end

local function updateAndDisplaySndBtn()
	soundsButtons.on.isVisible = false
	soundsButtons.off.isVisible = false
	if sounds.isSoundOn then
		soundsButtons.on.isVisible = true
	else
		soundsButtons.off.isVisible = true
	end
end

local function displayProfileCreator()
	if (connection == false) then
		print ("Not connected!")
		return false
	end


	local profilePage = display.newGroup()
		--local block = display.newRect()
    local block = display.newRect(profilePage, _CX, _CY, _CX * 7/4, _CY * 7/4)
	block.fill = {0.7, 1, 1}
	relayout.add(block)
	--profilePage:insert(block)

	local profileText = display.newText(profilePage, "Create Profile", _CX, 100, native.systemFontBold, 70)
    profileText:setFillColor( 0, 0, 0 )
    --profilePage.insert(profileText)

    local idText = display.newText(profilePage, "Player ID :", _CX*1/2, _CY *2/3, native.systemFont, 50)
    idText:setFillColor(0, 0, 0)
    --profilePage.insert(idText)

    local pwText = display.newText(profilePage, "Password : ", _CX*1/2, _CY, native.systemFont, 50)
    pwText:setFillColor(0, 0, 0)

    local function idListener( event )
        if ( event.phase == "editing") then

            userid = event.target.text
            print( userid )
        end
	end

	local function pwListener( event )
        if ( event.phase == "editing" ) then
            password = event.target.text
            print( password )
        end
	end
    -- Create text field
    idField = native.newTextField(_CX, _CY *2/3 + 2, 300, 55 )
    idField:addEventListener( "userInput", idListener )
    profilePage:insert(idField)

    pwField = native.newTextField( _CX, _CY + 2, 300, 55 )
    pwField:addEventListener( "userInput", pwListener )
    profilePage:insert(pwField)

    createBtn = widget.newButton {
		label="Create",
		labelColor = { default={255}, over={128} },
		fontSize = 50,
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=308, height=80,
		onRelease = function()
		    local userData = {}
		    userData.type = "createProfile"
		    userData.playerId = userid
		    userData.password = password
		    userData.playerWins = 0
		    encoded = encoder.encode(userData)
		    udp:send(encoded)
		    timer.performWithDelay(100, receiveUdpMsg)
		end	-- event listener function
	}
	createBtn.x = display.contentCenterX * 3/2 
	createBtn.y = display.contentHeight/2 + 175
    profilePage:insert(createBtn)

    local function closeBtnRelease()	
	    profilePage:removeSelf()
	    return true	-- indicates successful touch
    end

	closeBtn = widget.newButton {
		label="Close",
		labelColor = { default={255}, over={128} },
		fontSize = 50,
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=308, height=80,
		onRelease = closeBtnRelease	-- event listener function
	}
	closeBtn.x = display.contentCenterX * 1/2 
	closeBtn.y = display.contentHeight/2 + 175
	profilePage:insert(closeBtn)
end	

local function displayPlayerProfile()
	local playerNameText = display.newText( backView, "Hello, " .. playerProfile.playerId .. " !", _CX, _CY - 130, 
		native.systemFontBold, 40)
    playerNameText:setFillColor( 0, 0, 0 )
	local scoreText = display.newText( backView, "Wins : " .. playerProfile.playerWins, _CX, _CY - 80, 
		native.systemFontBold, 40)
    scoreText:setFillColor( 0, 0, 0 )
end

local function displayLogin()
	if (connection == false) then
		print ("Not connected!")
		return false
	end

	local profilePage = display.newGroup()
		--local block = display.newRect()
    local block = display.newRect(profilePage, _CX, _CY, _CX * 7/4, _CY * 7/4)
	block.fill = {0.7, 1, 1}
	relayout.add(block)
	--profilePage:insert(block)

	local profileText = display.newText(profilePage, "Login", _CX, 100, native.systemFontBold, 70)
    profileText:setFillColor( 0, 0, 0 )
    --profilePage.insert(profileText)

    local idText = display.newText(profilePage, "Player ID :", _CX*1/2, _CY *2/3, native.systemFont, 50)
    idText:setFillColor(0, 0, 0)
    --profilePage.insert(idText)

    local pwText = display.newText(profilePage, "Password : ", _CX*1/2, _CY, native.systemFont, 50)
    pwText:setFillColor(0, 0, 0)

    local function idListener( event )
        if ( event.phase == "ended" or event.phase == "submitted" ) then

            userid = event.target.text
            print( userid )
        end
	end

	local function pwListener( event )
        if ( event.phase == "ended" or event.phase == "submitted" ) then
            password = event.target.text
            print( password )
        end
	end
    -- Create text field
    idField = native.newTextField(_CX, _CY *2/3 + 2, 300, 55 )
    idField:addEventListener( "userInput", idListener )
    profilePage:insert(idField)

    pwField = native.newTextField( _CX, _CY + 2, 300, 55 )
    pwField:addEventListener( "userInput", pwListener )
    profilePage:insert(pwField)

    loginBtn = widget.newButton {
		label="Login",
		labelColor = { default={255}, over={128} },
		fontSize = 50,
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=308, height=80,
		onRelease = function()
		    local userData = {}
		    userData.type = "login"
		    userData.playerId = userid
		    userData.password = password
		    encoded = encoder.encode(userData)
		    udp:send(encoded)
		    timer.performWithDelay(100, receiveUdpMsg)
		end	-- event listener function
	}
	loginBtn.x = display.contentCenterX * 3/2 
	loginBtn.y = display.contentHeight/2 + 175
    profilePage:insert(loginBtn)

    local function closeBtnRelease()
    	if (profileDisplay == false and loggedIn == true) then
    	    displayPlayerProfile()
    	    profileDisplay = true
    	end	
	    profilePage:removeSelf()
	    return true	-- indicates successful touch
    end

	closeBtn = widget.newButton {
		label="Close",
		labelColor = { default={255}, over={128} },
		fontSize = 50,
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=308, height=80,
		onRelease = closeBtnRelease	-- event listener function
	}
	closeBtn.x = display.contentCenterX * 1/2 
	closeBtn.y = display.contentHeight/2 + 175
	profilePage:insert(closeBtn)
end	

function scene:create( event )
	local sceneGroup = self.view


	-- display a background image

	local background = display.newImageRect( sceneGroup, "images/background.jpg", display.actualContentWidth, 
		display.actualContentHeight )
	background.anchorX = 0
	background.anchorY = 0
	background.x = 0 + display.screenOriginX 
	background.y = 0 + display.screenOriginY



	-- create/position logo/title image on upper-half of the screen
	--[[
	local titleLogo = display.newImageRect( "images/logo.png", 264, 42 )
	titleLogo.x = display.contentCenterX
	titleLogo.y = 100]]
	local titleText = display.newText(sceneGroup, "Cat Fighters", display.contentCenterX, 
		100, native.systemFontBold, 70)
    titleText:setFillColor( 0, 0, 0 )




    playBtn = widget.newButton{
		label="Play",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=154, height=40,
		onRelease = onPlayBtnRelease	-- event listener function
	}
	playBtn.x = display.contentCenterX
	playBtn.y = display.contentHeight/2

	connectBtn = widget.newButton{
		label="Connect",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=154, height=40,
		onRelease = onConnectBtnRelease	-- event listener function
	}
	connectBtn.x = display.contentCenterX * 3/2 
	connectBtn.y = display.contentHeight/2 - 125

	newIDBtn = widget.newButton{
		label="Create ID",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=154, height=40,
		onRelease = displayProfileCreator	-- event listener function
	}
	newIDBtn.x = display.contentCenterX * 3/2 
	newIDBtn.y = display.contentHeight/2 - 65

	loginBtn = widget.newButton{
		label="Login",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=154, height=40,
		onRelease = displayLogin	-- event listener function
	}
	loginBtn.x = display.contentCenterX * 3/2 
	loginBtn.y = display.contentHeight/2 - 5

	settingsBtn = widget.newButton{
		label="Settings",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=154, height=40,
		--onRelease = onPlayBtnRelease	-- event listener function
	}
	settingsBtn.x = display.contentCenterX * 3/2 
	settingsBtn.y = display.contentHeight/2 + 55

    
	instructionsBtn = widget.newButton {
		label="Instructions",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=154, height=40,
		--onRelease = onPlayBtnRelease	-- event listener function
	}
	instructionsBtn.x = display.contentCenterX * 3/2 
	instructionsBtn.y = display.contentHeight/2 + 115


	creditsBtn = widget.newButton {
		label="Credits",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=154, height=40,
		--onRelease = onPlayBtnRelease	-- event listener function
	}
	creditsBtn.x = display.contentCenterX * 3/2 
	creditsBtn.y = display.contentHeight/2 + 175


	soundsButtons.on = widget.newButton({
		defaultFile = 'images/buttons/sounds_on.png',
		overFile = 'images/buttons/sounds_on-over.png',
		width = 48, height = 48,
		x = display.screenOriginX + 30, y = display.contentHeight - 30,
		onRelease = function()
			sounds.play('tap')
			sounds.isSoundOn = false
			updateAndDisplaySndBtn()
		end
	})
	soundsButtons.on.isRound = true

	soundsButtons.off = widget.newButton({
		defaultFile = 'images/buttons/sounds_off.png',
		overFile = 'images/buttons/sounds_off-over.png',
		width = 48, height = 48,
		x = display.screenOriginX + 30, y = display.contentHeight - 30,
		onRelease = function()
			sounds.play('tap')
			sounds.isSoundOn = true
			updateAndDisplaySndBtn()
		end
	})
	soundsButtons.off.isRound = true


	
	-- all display objects must be inserted into group
	--sceneGroup:insert( background )
	--sceneGroup:insert( titleLogo )
	sceneGroup:insert( creditsBtn)
	sceneGroup:insert( instructionsBtn)
	sceneGroup:insert( settingsBtn)
	sceneGroup:insert( loginBtn)
	sceneGroup:insert( newIDBtn)
	sceneGroup:insert( connectBtn)
	sceneGroup:insert( playBtn )
	sceneGroup:insert( soundsButtons.on )
	sceneGroup:insert( soundsButtons.off )
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		backView:removeSelf()
	    backView = nil
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end


function scene:destroy( event )
	local sceneGroup = self.view
	
	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.

	
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene