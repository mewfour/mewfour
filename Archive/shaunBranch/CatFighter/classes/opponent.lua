-- Opponent Class
-- Opponent object in the game and contains functions to be called
-- by the game class.
-- Opponent will be controlled as a 'reflection' of the other
-- client's movement over the server. 

local physics = require('physics')
local relayout = require('libs.relayout')

local _M = {}

local _W, _H, _CX, _CY = relayout._W, relayout._H, relayout._CX, relayout._CY

-- Configurations for image sheet
local options = {
   	width = 64,
   	height = 64,
   	numFrames = 256
}

-- image sheet of opponent sprite
local imageSheet = graphics.newImageSheet( "images/cat_sprite/cat_full.png", options )

-- Sequence data for opponent sprite
local sequenceData =
{
	-- idle mode frames
	{
    name="idle",
    start=1,
    count=4,
    time= 500,
    loopCount = 0,   -- Optional ; default is 0 (loop indefinitely)
    loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
	},
	-- walking frames
	{
    name="walking",
    start=17,
    count=8,
    time= 1000,
    loopCount = 0,   -- Optional ; default is 0 (loop indefinitely)
    loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
	},
	-- jumping frames
	{
    name="jumping",
    start=33,
    count=8,
    time= 800,
    loopCount = 1,   -- Optional ; default is 0 (loop indefinitely)
    loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
	}
}

function _M.newOpponent(params)
	local opponent = display.newSprite( imageSheet, sequenceData )

	-- Initial direction in which the sprite is facing based on params
	opponent.direction = params.dir
	if (opponent.direction == "left") then
    	opponent:scale( -params.scale, params.scale)
    else 
    	opponent:scale( params.scale, params.scale)
   	end

	opponent.anchorY = 1
	opponent.x, opponent.y = params.x, params.y

	-- Check if opponent is allowed to jump
	opponent.canJump = 0

	-- Check if opponent is in contact with the ground
	opponent.inAir = false

	-- Check if opponent is moving or idle
	opponent.moving = false

	-- Sprite is set to idle animation when not moving
	opponent:setSequence( "idle" )
	opponent:play()

	-- rectangular physics area for the sprite
	local rectangleShape = { -15,10, 15,10, 15,-65, -15,-65}

	-- Describing parameters for physics body
	local bodyParams = {shape = rectangleShape, density = 20000, friction = 0, bounce = 0}
	physics.addBody(opponent, 'dynamic', bodyParams)
	opponent.isFixedRotation = true

	-- Collision function which describes the collision of the opponent
	-- with the ground. 
	-- Output: jumping boolean and inAir boolean
	function charCollide( self,event )
   		if ( event.selfElement == 1 and event.other.objType == "ground" ) then
    		if ( event.phase == "began") then
        		self.canJump = self.canJump+1
        		if (opponent.inAir == true) then
        			opponent:setSequence("idle")
					opponent:play()
				end
				opponent.inAir = false
      		elseif ( event.phase == "ended" ) then
         		self.canJump = self.canJump-1
     		end
   		end
	end
	opponent.collision = charCollide 
	opponent:addEventListener("collision", opponent)	

	-- Jump function 
	function opponent:jump()
		opponent.inAir = true
		opponent.x = opponent.x + 5
		opponent:setLinearVelocity(0, -1300)
		opponent:setSequence("jumping")
		opponent:play()
	end

	-- Function to move right if requested
	function opponent:moveRight()
			if (opponent.direction == "left") then
				opponent.direction = "right"
				opponent.xScale = 2.5
			end
			opponent.x = opponent.x + 5
			if (opponent.sequence == "idle") then
				opponent:setSequence("walking")
				opponent:play()
			end
	end

	-- Function to move left if requested
	function opponent:moveLeft()
			if (opponent.direction == "right") then
				opponent.direction = "left"
				opponent.xScale = -2.5
			end
			opponent.x = opponent.x - 5
			if (opponent.sequence == "idle") then
				opponent:setSequence("walking")
				opponent:play()
			end
	end

	-- Sets opponent sprite to idle mode if not moving
	function opponent:setIdle()
		opponent:setSequence("idle")
		opponent:play()
	end

	return opponent
end

return _M