-- Player Class
-- Contains the main player in the game. 
-- Functions included movement configurations
-- Will send message to udp entered in params

local physics = require('physics')
local relayout = require('libs.relayout')
local json = require('json')
local encoder = require('libs.easyEncoder')

local _M = {}

local _W, _H, _CX, _CY = relayout._W, relayout._H, relayout._CX, relayout._CY

-- Configurations for image sheet
local options = {
   	width = 64,
   	height = 64,
   	numFrames = 256
}

-- image sheet of player sprite
local imageSheet = graphics.newImageSheet( "images/cat_sprite/cat_full.png", options )

-- Sequence data for player sprite
local sequenceData =
{
	-- idle mode frames
	{
    name="idle",
    start=1,
    count=4,
    time= 500,
    loopCount = 0,   -- Optional ; default is 0 (loop indefinitely)
    loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
	},
	-- walking frames
	{
    name="walking",
    start=17,
    count=8,
    time= 500,
    loopCount = 0,   -- Optional ; default is 0 (loop indefinitely)
    loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
	},
	-- jumping frames
	{
    name="jumping",
    start=33,
    count=8,
    time= 800,
    loopCount = 1,   -- Optional ; default is 0 (loop indefinitely)
    loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
	}
}

function _M.newPlayer(params)
	local player = display.newSprite( imageSheet, sequenceData )

	-- Initial direction in which the sprite is facing based on params
	player.direction = params.dir
	if (player.direction == "right") then
    	player:scale( params.scale, params.scale)
    else
    	player:scale( -params.scale, params.scale)
    end

	player.anchorY = 1
	player.x, player.y = params.x, params.y

	-- Check if player is allowed to jump
	player.canJump = 0

	-- Check if player is in contact with the ground
	player.inAir = false

	-- Check if player jumped
	player.jumped = false

	-- player is set to idle animation when not moving
	player:setSequence( "idle" )
	player:play()

	-- Check which key is currently active
	player.keyRight = false
	player.keyLeft = false

	-- Check if player was moving previously before idling
	player.wasMoving = false


	-- rectangular physics area for the sprite
	local rectangleShape = { -15,10, 15,10, 15,-65, -15,-65}

	-- Describing parameters for physics body
	local bodyParams = {shape = rectangleShape, density = 20000, friction = 0, bounce = 0}
	physics.addBody(player, 'dynamic', bodyParams)
	player.isFixedRotation = true


	-- Collision function which describes the collision of the opponent
	-- with the ground. 
	-- Output: jumping boolean and inAir boolean
	function charCollide( self,event )
   		if ( event.selfElement == 1 and event.other.objType == "ground" ) then
    		if ( event.phase == "began") then
        		self.canJump = self.canJump+1
        		if (player.inAir == true) then
        			player:setSequence("idle")
					player:play()
				end
				player.inAir = false
      		elseif ( event.phase == "ended" ) then
         		self.canJump = self.canJump-1
     		end
   		end
	end
	player.collision = charCollide 
	player:addEventListener("collision", player)

	-- function to move right if requested
	function moveRight(event)
		if (player.keyRight == true and player.x < _W - 32) then
			player.x = player.x + 5
			if (player.sequence == "idle") then
				player:setSequence("walking")
				player:play()
			end
		end
	end

	-- function to move left if requested
	function moveLeft(event)
		if (player.keyLeft == true and player.x > 32) then
			player.x = player.x - 5
			if (player.sequence == "idle") then
				player:setSequence("walking")
				player:play()
			end
		end

	end

	-- Function to move sprite based on input keys
	function onKeyEvent(event)

		-- Right keypad, Activated only if "left" key is not active
		if (event.keyName == "right" and player.keyLeft == false) then
			-- flip sprite if it was facing opposite direction
			if (player.direction == "left") then
				player.direction = "right"
				player.xScale = 2.5
			end

			-- key is being held down
			if (event.phase == "down") then
				player.keyRight = true

			-- key is lifted
			else
				player.wasMoving = true
				player.keyRight = false				
				if (player.inAir == false) then
					player:setSequence( "idle" )
					player:play()
				end
			end

		-- Left keypad, Activated only if "right" key is not active
		elseif (event.keyName == "left" and player.keyRight == false) then
			-- flip sprite if it was facing opposite direction
			if (player.direction == "right") then
				player.direction = "left"
				player.xScale = -2.5
			end

			-- key is being held down
			if (event.phase == "down") then
				player.keyLeft = true

			-- key is lifted
			else
				player.wasMoving = true
				player.keyLeft = false
				if (player.inAir == false) then
					player:setSequence( "idle" )
					player:play()
				end
			end
		end

		-- Spacebar for jump
		if (event.keyName == "space") then
			if (player.canJump > 0  and event.phase == "down") then
				player.jumped = true
				player.inAir = true
				player:setLinearVelocity(0, -1300)
				player:setSequence("jumping")
				player:play()
			end
		end
	end

	-- Adding the movements as runtime listeners
	Runtime:addEventListener( "enterFrame", moveRight)
	Runtime:addEventListener( "enterFrame", moveLeft)
	Runtime:addEventListener("key", onKeyEvent)

	-- Encodes message and send it via udp
	function send_message(t)
		encoded_message = encoder.encode(t)
    	udp:send(encoded_message)
	end

	-- Sends message if updated (per frame)
	-- message structure:
	-- direction = direction in which player is moving (if active)
	-- jump = player jumped
	-- send_coords = boolean: sends coordinates if player is moving and not jumping
	-- available = boolean: sends message if there is an update
	function periodic_send(event)
		--local player_coordinates = {x = player.x, y = player.y}

		-- message structure
		local message = {direction = "none", 
							jump = "none", 
							x = player.x,
							y = player.y, 
							send_coords = "true",
							available = "false"}

		-- update direction
		if (player.keyRight == true) then
			message.direction = "right"
			message.available = "true"
		elseif (player.keyLeft == true) then
			message.direction = "left"
			message.available = "true"
		end

		-- update jump
		if (player.jumped == true) then
			message.jump = "jump"
			player.jumped = false
			message.available = "true"
		end

		-- update send_coords boolean
		if (player.inAir == true) then
			send_coords = "false"
		end

		-- activates idle mode (if player was moving and now stopped)
		if (player.wasMoving == true and player.inAir == false) then
			message.available = "true"
			message.direction = "idle"
			player.wasMoving = false
		end

		-- update available, send message.
		if (message.available == "true") then
			send_message(message)
		end
	end
	Runtime:addEventListener( "enterFrame", periodic_send)

	return player
end

return _M
