-- Menu Scene 
-- Displays game's name, the cannon and a couple buttons.

-- MAIN SCENE BEFORE GAMEPLAY, THE FIRST STATIONARY SCENE SEEN BY PLAYER
local composer = require('composer')
local physics = require('physics') -- Box2D physics
local widget = require('widget')
local relayout = require('libs.relayout')
local scene = composer.newScene()
local socket = require("socket")
local newPlayer = require('classes.player').newPlayer
physics.start()
physics.setGravity(0, 150) -- Default gravity is too boring


function scene:create()
	-- BACKGROUND SETTINGS
	local _W, _H, _CX, _CY = relayout._W, relayout._H, relayout._CX, relayout._CY

   udp=socket.udp()
   udp:setsockname("192.168.1.2", 12345)
    udp:settimeout(0)
    --udp:setsockname(ip, port)
--local data
 
local function receiveUdpMsg(event)
    --print("R")
    data, ip, port = udp:receivefrom()
    if data then
        print("*", data, ip, port)
        udp:sendto(data, ip, port)

        		if (data == "jump") then
        			self.player1:jump()
        			print("jump")
        		end
        		if (data == "right") then
        			self.player1:moveRight()
        			print("right")
        		end
        		if (data == "left") then
        			self.player1:moveLeft()
        			print("left")
        		end

    end
    --socket.sleep(0.01)
     --recv= timer.performWithDelay(5, receiveUdpMsg)
end
 
--recv= timer.performWithDelay(100, receiveUdpMsg)
Runtime:addEventListener( "enterFrame", receiveUdpMsg)

	--
	-- A FUNCTION THAT WILL CLOSE NETWORK A CONNECTION TO PUBNUB
	--


	local group = self.view

	-- BACKGROUND SETTINGS , DISPLAYING COLOUR AND RECTANGLE
	local background = display.newRect(group, _CX, _CY, _W, _H)
	background.fill = {
	    type = 'gradient',
	    color1 = {0.2, 0.45, 0.8},
	    color2 = {0.7, 0.8, 1}
	}
	relayout.add(background)

	-- Creating bottom platform
	self.tiles = {}
	local numTiles = math.ceil(_W / 64 / 2)

	-- tileShape is the thin rectangular section which defines the top area of 
	-- the platform. 
	local tileShape = { -24,-30, 24,-30, 24,-32, -24,-32}
	local tileParams = {shape = tileShape, density = 2, friction = 0, bounce = 0}
	for i = -numTiles - 4, numTiles + 4 do -- Add extra 4 on the sides for resize events
		--local tileblock = display.newImageRect(group, 'images/green_tiles/3.png', 64, 64)
		local tile = display.newImageRect(group, 'images/green_tiles/3.png', 64, 64)
		tile.anchorY = 1
		tile.x, tile.y = i * 64 + _CX, _H
		tile.objType = "ground"
		physics.addBody(tile, 'static', tileParams)
		table.insert(self.tiles, tile)
	end

	-- top left platform
	-- tileblock is the overlapping platform that considers the whole platform as a physical object
	local tileBlockParams = {density = 2, friction = 0, bounce = 0}
	for i = -numTiles, numTiles/2 - 8 do -- Add extra 4 on the sides for resize events
		local tile = display.newImageRect(group, 'images/green_tiles/3.png', 64, 64)
		local tileblock = display.newImageRect(group, 'images/green_tiles/3.png', 64, 64)
		tile.anchorY = 1
		tileblock.anchorY = 1
		tile.x, tile.y = i * 64 + _CX, _CY* 3/2
		tileblock.x, tileblock.y = i * 64 + _CX, _CY* 3/2
		tile.objType = "ground"
		physics.addBody(tile, 'static', tileParams)
		physics.addBody(tileblock, 'static', tileBlockParams)
		table.insert(self.tiles, tile)
	end

	-- top right platform
	-- tileblock is the overlapping platform that considers the whole platform as a physical object
	for i = numTiles/2, numTiles do -- Add extra 4 on the sides for resize events
		local tile = display.newImageRect(group, 'images/green_tiles/3.png', 64, 64)
		local tileblock = display.newImageRect(group, 'images/green_tiles/3.png', 64, 64)
		tile.anchorY = 1
		tileblock.anchorY = 1
		tile.x, tile.y = i * 64 + _CX, _CY* 3/2
		tileblock.x, tileblock.y = i * 64 + _CX, _CY* 3/2
		tile.objType = "ground"
		physics.addBody(tile, 'static', tileParams)
		physics.addBody(tileblock, 'static', tileBlockParams)
		table.insert(self.tiles, tile)
	end

	--connect()
	-- create player
	self.player1 = newPlayer({g = group, scale = 2.5, x = 64, y = _H - 128, channel = CHAT_CHANNEL, playerChat = chat})



end

scene:addEventListener('create')
--scene:addEventListener('show')
--scene:addEventListener('hide')

return scene