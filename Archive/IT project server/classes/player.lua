-- Player Class
local physics = require('physics')
local relayout = require('libs.relayout')

local _M = {}

local _W, _H, _CX, _CY = relayout._W, relayout._H, relayout._CX, relayout._CY

-- Configurations for image sheet
local options = {
   	width = 64,
   	height = 64,
   	numFrames = 256
}

-- image sheet of player sprite
local imageSheet = graphics.newImageSheet( "images/cat_sprite/cat_full.png", options )

-- Sequence data for player sprite
local sequenceData =
{
	-- idle mode frames
	{
    name="idle",
    start=1,
    count=4,
    time= 500,
    loopCount = 0,   -- Optional ; default is 0 (loop indefinitely)
    loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
	},
	-- walking frames
	{
    name="walking",
    start=17,
    count=8,
    time= 1000,
    loopCount = 0,   -- Optional ; default is 0 (loop indefinitely)
    loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
	},
	-- jumping frames
	{
    name="jumping",
    start=33,
    count=8,
    time= 800,
    loopCount = 1,   -- Optional ; default is 0 (loop indefinitely)
    loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
	}
}

function _M.newPlayer(params)
	local player = display.newSprite( imageSheet, sequenceData )
    player:scale( params.scale, params.scale)
	player.anchorY = 1
	player.x, player.y = params.x, params.y
	player.canJump = 0
	player.direction = "right"
	player.inAir = false

	-- player is set to idle animation when not moving
	player:setSequence( "idle" )
	player:play()


	-- rectangular physics area for the sprite
	local rectangleShape = { -15,10, 15,10, 15,-65, -15,-65}

	-- Describing parameters for physics body
	local bodyParams = {shape = rectangleShape, density = 20000, friction = 0, bounce = 0}
	physics.addBody(player, 'dynamic', bodyParams)
	player.isFixedRotation = true
	--player :addEventListener( "sprite", mySpriteListener ) 
	function charCollide( self,event )
   	if ( event.selfElement == 1 and event.other.objType == "ground" ) then
    	if ( event.phase == "began") then
        	self.canJump = self.canJump+1
        	if (player.inAir == true) then
        		player:setSequence("idle")
				player:play()
			end
			player.inAir = false
      	elseif ( event.phase == "ended" ) then
         	self.canJump = self.canJump-1
     	end
   	end
	end
	player.collision = charCollide 
	player:addEventListener("collision", player)



	-- function to jump if requested
	-- unused atm
	--function jump(event)
		--if (player.canJump > 0) then
			--player:setLinearVelocity(0, -1200)
		--end
	--end
	-- function that response to input keys for movement and jump.
	-- boolean exists to allow 'holding' for continuous movement

	function onKeyEvent(event)
		if (event.keyName == "right") then
			-- flip sprite if it was facing opposite direction
			if (player.direction == "left") then
				player.direction = "right"
				player.xScale = 2.5
			end
			if (event.phase == "down") then
				keyRight = true
			else
				keyRight = false				
				if (player.inAir == false) then
					player:setSequence( "idle" )
					player:play()
				end
			end
		elseif (event.keyName == "left") then
			-- flip sprite if it was facing opposite direction
			if (player.direction == "right") then
				player.direction = "left"
				player.xScale = -2.5
			end
			if (event.phase == "down") then
				keyLeft = true
				player:setSequence( "walking" )
				player:play()
			else
				keyLeft = false
				if (player.inAir == false) then
					player:setSequence( "idle" )
					player:play()
				end
			end
		end
		if (event.keyName == "space") then
			if (player.canJump > 0  and event.phase == "down") then
				player.inAir = true
				player:setLinearVelocity(0, -1300)
				player:setSequence("jumping")
				player:play()
			end
		end
	end

	function player:jump()
		player.inAir = true
		player.x = player.x + 5
		player:setLinearVelocity(0, -1300)
		player:setSequence("jumping")
		player:play()
	end


	-- function to move right if requested
	function player:moveRight()
			player.x = player.x + 5
			if (player.sequence == "idle") then
				player:setSequence("walking")
				player:play()
			end
	end

	function player:moveLeft()
			player.x = player.x - 5
			if (player.sequence == "idle") then
				player:setSequence("walking")
				player:play()
			end
	end
	-- Adding the movements as runtime listeners
	--Runtime:addEventListener( "enterFrame", moveRight)
	--Runtime:addEventListener( "enterFrame", moveLeft)
	--Runtime:addEventListener( "enterFrame", jump)
	--Runtime:addEventListener("key", onKeyEvent)
	return player
end

return _M