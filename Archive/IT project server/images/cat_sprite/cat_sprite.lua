--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:570ddc1ea52678803d68d6dccbc827ee:16ea9839b825dbee1c53c543bc3fccd7:a1a850acc110ee041856fa14679c445d$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- cat_full
            x=1,
            y=1,
            width=988,
            height=994,

        },
    },
    
    sheetContentWidth = 990,
    sheetContentHeight = 996
}

SheetInfo.frameIndex =
{

    ["cat_full"] = 1,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
