--Controller class

local composer = require('composer')
local widget = require('widget')
local relayout = require('libs.relayout')

local _M = {}

local health
local healthBar


function _M.newController(params)
	local _W,_H, _CX, _CY = relayout._W, relayout._H, relayout._CX, relayout._CY

	local controller = display.newGroup()

	params.g:insert(controller)

	local visualButton = {}

	local health = 100
	local healthBar

	healthBar = display.newRect(display.screenOriginX + health  + 10,display.screenOriginY + 10, health * 2, 10)

	healthBar:setFillColor( 000/255, 255/255, 0/255 )
	healthBar.strokeWidth = 1
	healthBar:setStrokeColor( 255, 255, 255, .5 )

	controller:insert(healthBar)

	local rtnBtn = widget.newButton{
		defaultFile = "images/buttons/restart.png",
		overFile = "images/buttons/restart-over.png",
		width = 40, height = 40,
		x = _W - 30 , y = 30,
		onRelease = function ()
		composer.gotoScene("scenes.menu","fade",500)
		end
	}

	controller:insert(rtnBtn)
	table.insert(visualButton,rtnBtn)


	local leftBtn = widget.newButton{
		defaultFile = "images/buttons/left.png",
		overFile = "images/buttons/left.png",
		width = 96, height = 96,
		x = 48, y = _H - 50,
		onPress = function (params)
		print("PressL")
		print(params.kl)
		params.kr = true
		print(params.kl)

		end
	}

	controller:insert(leftBtn)
	table.insert(visualButton,leftBtn)

	local rightBtn = widget.newButton{
		defaultFile = "images/buttons/right.png",
		overFile = "images/buttons/right.png",
		width = 96, height = 96,
		x = leftBtn.x + 100, y = _H - 50,
		onPress = function(params)
		print("PressR")
		print(params.kr)
		params.kr = true
		print(params.kr)

		end
	}

	controller:insert(rightBtn)
	table.insert(visualButton,rightBtn)

	function controller:relayout()
		controller.y = relayout._CY
	end

	return controller
end

return _M

