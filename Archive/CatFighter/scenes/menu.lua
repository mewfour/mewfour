-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
-- local sidebar = require("classes.sidebar").newSidebar

local relayout = require('libs.relayout')

local scene = composer.newScene()

-- include Corona's "widget" library
local widget = require "widget"

local sounds = require('libs.sounds')

local s = require("socket")
udp = s.udp()

udp:settimeout(1)

--------------------------------------------

-- forward declarations and other locals
local playBtn
local soundsButtons = {}
local musicButtons = {}

-- 'onRelease' event listener for playBtn
local function onPlayBtnRelease()
	
	--local address, port = "59.102.43.179", 12345
	local address, port = "10.13.113.105", 12345
	--local address, port = "10.12.253.195", 12345
	udp:setpeername(address, port)
	udp:send("connect")

	-- go to level1.lua scene
	composer.gotoScene( "scenes.game", "fade", 500 )
	
	return true	-- indicates successful touch
end


local function updateAndDisplaySndBtn()

	soundsButtons.on.isVisible = false
	soundsButtons.off.isVisible = false
	if sounds.isSoundOn then
		soundsButtons.on.isVisible = true
	else
		soundsButtons.off.isVisible = true
	end
end



function scene:create( event )
	local sceneGroup = self.view

	local _W, _CX, _CY = relayout._W, relayout._CX, relayout._CY

	-- Called when the scene's view does not exist.
	-- 
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.


	-- display a background image
	local background = display.newImageRect( "images/background.jpg", display.actualContentWidth, display.actualContentHeight )
	background.anchorX = 0
	background.anchorY = 0
	background.x = 0 + display.screenOriginX 
	background.y = 0 + display.screenOriginY
	
	-- create/position logo/title image on upper-half of the screen
	local titleLogo = display.newImageRect( "images/logo.png", 264, 42 )
	titleLogo.x = display.contentCenterX
	titleLogo.y = 100
	
	-- create a widget button (which will loads level1.lua on release)
	playBtn = widget.newButton{
		label="Play Now",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		width=154, height=40,
		onRelease = onPlayBtnRelease	-- event listener function
	}
	playBtn.x = display.contentCenterX
	playBtn.y = display.contentHeight - 125


	soundsButtons.on = widget.newButton({
		defaultFile = 'images/buttons/sounds_on.png',
		overFile = 'images/buttons/sounds_on-over.png',
		width = 48, height = 48,
		x = display.screenOriginX + 30, y = display.contentHeight - 30,
		onRelease = function()
			sounds.play('tap')
			sounds.isSoundOn = false
			updateAndDisplaySndBtn()
		end
	})
	soundsButtons.on.isRound = true

	soundsButtons.off = widget.newButton({
		defaultFile = 'images/buttons/sounds_off.png',
		overFile = 'images/buttons/sounds_off-over.png',
		width = 48, height = 48,
		x = display.screenOriginX + 30, y = display.contentHeight - 30,
		onRelease = function()
			sounds.play('tap')
			sounds.isSoundOn = true
			updateAndDisplaySndBtn()
		end
	})
	soundsButtons.off.isRound = true

	

	
	-- all display objects must be inserted into group
	sceneGroup:insert( background )
	sceneGroup:insert( titleLogo )
	sceneGroup:insert( playBtn )
	sceneGroup:insert( soundsButtons.on )
	sceneGroup:insert( soundsButtons.off )
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end


function scene:destroy( event )
	local sceneGroup = self.view
	
	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	
	if playBtn then
		playBtn:removeSelf()	-- widgets must be manually removed
		playBtn = nil
	end
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene